<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Auth::routes();
Route::get('/', 'GeneralController@index');

Route::get('/home', 'HomeController@index');
Route::resource('transactions','TransactionsController');
//payments
Route::resource('payments','PaymentController');

//admin-dashboard
Route::get('dashboard','DashboardController@index');
//bus-operators
Route::resource('bus-operators','BusOperatorController');
//routes
Route::resource('routes','RouteController');
//montlyplan
Route::resource('monthly-plan','MonthlyPlanController');
//tickets
Route::get('ticket','TicketController@buyTicket');
Route::post('ticket/buy','TicketController@payMent');
Route::post('ticket/fare','TicketController@getFare');
Route::get('ticket/my-tickets','TicketController@myTickets');

//monthly-plan
Route::post('monthly-plan/cost','PlanTransactionController@cost');
Route::post('monthly-plan/subscription','PlanTransactionController@monthlyPlanSubs');
//contact
Route::get('/contact','ContactController@index');
//about us
Route::get('/who-we-are','AboutController@whoWeAre');
Route::get('/our-team','AboutController@ourTeam');
//statements
Route::get('/statements','SinglePagesController@statements');
//user
Route::get('/edit-account','UserController@editUserAccount');
Route::post('/updateUserAccount','UserController@updateUserAccount');


