<?php

use App\RouteStage;
use Illuminate\Database\Seeder;

class RouteStageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();
        RouteStage::insert(
            [
                [
                    'route_id'=>1,
                    'name'=>'Utawala',
                    'location'=>'utawala',
                    'created_at' => $now,
                    'updated_at' => $now
                ]
                ,
                [
                    'route_id'=>1,
                    'name'=>'Ambassador',
                    'location'=>'nairobi cbd',
                    'created_at' => $now,
                    'updated_at' => $now
                ]

            ]
        );
    }
}
