<?php

use App\Fare;
use Illuminate\Database\Seeder;

class FareSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();
        Fare::insert(
            [
            [
                'from_id'=>1,
                'to_id'=>2,
                'fare'=>80.0,
                'created_at'=>$now,
                'updated_at' => $now
            ],
            [
                'from_id'=>1,
                'to_id'=>2,
                'fare'=>80.0,
                'created_at'=>$now,
                'updated_at' => $now
            ]
            ]
        );
    }
}
