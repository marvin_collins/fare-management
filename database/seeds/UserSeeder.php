<?php

use App\User;
use App\Wallet;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            [
                'name' => 'kbs',
                'email' => 'kbs@demo.com',
                'password' => bcrypt('kbs1234'),
                'national_id' => 30025422,
                'phone' => '0704407117',
                'username' => 'kbs'
            ]
        );

        Wallet::create(
            [
                'user_id' => 1,
                'account_number'=>rand(0704407117,12),
                'balance' => 0.0
            ]
        );
    }
}
