<?php

use App\Route;
use Illuminate\Database\Seeder;

class RouteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Route::create(
            [
                'name'=>'Utawala - Nairobi CBD (town)',
                'description'=>'This is route from Utwala to Nairobi CBD(town)',
                'route'=>'33 - Mombasa road'
            ]
        );
    }
}
