<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(RouteSeeder::class);
        $this->call(RouteStageSeeder::class);
        $this->call(FareSeeder::class);
        $this->call(PlanSeeder::class);
    }
}
