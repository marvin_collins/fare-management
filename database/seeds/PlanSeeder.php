<?php

use App\Plan;
use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Plan::create(
            [
                'name'=>'Monthly Plan Mon - Fri',
                'description'=>'Monthly Plan Mon - Fri',
                'number_of_days' => 5,
                'discount'=>0.10
            ]
        );
        Plan::create(
            [
                'name'=>'Monthly Plan Mon - Sat',
                'description'=>'Monthly Plan Mon - Sat',
                'number_of_days' => 6,
                'discount'=>0.11
            ]
        );
        Plan::create(
            [
                'name'=>'Monthly Plan Mon - Sun',
                'description'=>'Monthly Plan Mon - Sun',
                'number_of_days' => 7,
                'discount'=>0.12
            ]
        );
    }
}
