<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monthly_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsinged();
            $table->integer('route_id')->unsinged();
            $table->integer('plan_id')->unsigned();
            $table->string('plan_type');
            $table->integer('terminal_one')->unsinged();
            $table->integer('terminal_two')->unsinged();
            $table->bigInteger('plan_count');
            $table->string('reference');
            $table->date('plan_end_date');
            $table->date('plan_start_date');
            $table->double('amount',2);
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthly_plans');
    }
}
