@extends('layout')
@section('content')
    <div class="container">
        <div class="row" style="margin-top: 20px">
            @foreach($errors as $error)
                <p>{{$error}}</p>
            @endforeach
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><h3>Buy Ticket</h3></div>
                    <div class="panel-body">
                        <form role="form" method="POST" id="buy_ticket">
                            {{ csrf_field() }}
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-sm-6 pull-right">
                                        <div class="well well-sm text-center well-dafault" >
                                            Account Balance: Ksh: {{$wallet->balance}}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group form-control-material static required">
                                            <select onchange="addRoute(this,{{$routes}})" name="route" id="route" class="form-control inputs" data-toggle="select2"
                                                    data-style="btn-white" data-live-search="true" data-size="5">
                                                <option value="" >Select route</option>
                                                @foreach($routes as $rte)
                                                     <option value="{{$rte->id}}">{{$rte->route}}</option>
                                                @endforeach

                                            </select>
                                        <label for="route">Route</label>
                                    </div>
                                    <div class="form-group form-control-material static required">
                                            <select name="pickup" onchange="stationUpdate('pickup')" id="pickup" class="form-control inputs" data-toggle="select2"
                                                    data-style="btn-white" data-live-search="true" data-size="5">
                                                <option value="" >Select pickup</option>
                                            </select>
                                        <label for="route">Pick Up Stage</label>
                                    </div>

                                    <div class="form-group form-control-material static required">
                                        <select name="destination" id="destination" onchange="addDestination()" class="form-control inputs" data-toggle="select2"
                                                data-style="btn-white" data-live-search="true" data-size="5">
                                            <option value="" >Select destination</option>
                                        </select>
                                    <label for="route">Destination</label>
                                </div>
                                </div>
                                <br>

                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading"><h4 class="text-center" style="color: inherit">
                                            Ticket Summary</h4></div>
                                    <div class="panel-body">
                                        <table class="table table-stripped table-condensed">
                                            <tr>
                                                <th>Route</th>
                                                <td id="t_route">Select route</td>
                                            </tr>
                                            <tr>
                                                <th>Pick-up</th>
                                                <td id="t_pickup">Select pick up station</td>
                                            </tr>
                                            <tr>
                                                <th>Destination</th>
                                                <td id="t_destination">Select destination station</td>
                                            </tr>
                                            <tr>
                                                <th>Ticket No:</th>
                                                <td id="t_ticket">{{$ticket}}</td>
                                            </tr>
                                            <tr>
                                                <th>Fare</th>
                                                <td >Ksh: <span id="t_fare">0.0</span></td>
                                            </tr>
                                        </table>
                                        <div id="payment_btn" class="col-sm-12 text-center">
                                        @if($wallet->balance > 20)
                                        <button type="submit" onclick="checkError()" class="btn btn-xs btn-primary">
                                            Buy Ticket
                                        </button>
                                        @else
                                        <button type="button" class="btn btn-xs btn-circle btn-stroke btn-warning">
                                            OR</button>
                                        <button type="button" data-target="#myModal" data-toggle="modal" class="btn btn-xs pull-right btn-success">
                                            Pay with M-Pesa
                                        </button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <a href="{{url('/')}}" class="btn btn-circle btn-stroke btn-primary">
                                        <i class="fa fa-fw fa-arrow-left"></i>
                                    </a>
                                </div>
                                <div class="col-sm-8 pull-right">
                                    <pre hidden id="error" style="color: red; margin: 2px">Your account balance is insufficient</pre>
                                </div>
                                {{--<div class="col-sm-6">--}}
                                {{--<button type="button" style="margin: 5px" class="btn btn-primary pull-right">Top Up</button>--}}
                                {{--</div>--}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('transactions.store') }}">
                <div class="modal-content">
                    <div class="modal-header btn-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center" id="myModalLabel" style="color: inherit">Pay with M-Pesa</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h4>Instructions</h4>
                                <ol>
                                    <li>Go to M-PESA Menu</li>
                                    <li>Select Pay Bill</li>
                                    <li>Enter 552552 as the Business Number</li>
                                    <li>Enter 123456 as the Account Number</li>
                                    <li>Enter the value amount to pay eg 3000 (NO COMMAS)</li>
                                    <li>Enter your M-PESA PIN</li>
                                    <li>Then send the request</li>
                                    <li>You will receive an SMS confirming the transaction</li>
                                    <li>Enter the M-PESA REFERENCE NUMBER below</li>
                                    <li>Click finish button below to finish</li>
                                </ol>
                            </div>
                        </div>

                            {{ csrf_field() }}
                            <div class="col-md-12">
                                <div class="form-group form-control-material static required {{ $errors->has('reference') ? ' has-error' : '' }}">
                                    <input id="reference" type="text" placeholder="Enter M-Pesa Reference" class="form-control text-uppercase" name="reference" value="{{ old('reference') }}" required autofocus>
                                    <label for="reference">M-PESA REFERENCCE NUMBER
                                    </label>
                                </div>
                            </div>
                        <button type="button" id="dismiss" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn pull-right btn-sm btn-primary btn-primary">Finish</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@include('partials.footer')
@endsection
@section('scripts')
    <script>
//        $("#error").hide();
        $('#buy_ticket').on('submit', function (e) {
            e.preventDefault();
            if(valitaeForm())
            {
                buyTicket();
            }

        });

function valitaeForm() {
    if($("#route").val().length < 1)
    {
        showErrors('Select route');
        $("#t_fare").text('0.0');
        return false;
    }
    else if($("#pickup").val().length < 1)
    {
        showErrors('Select pickup station');
        $("#t_pickup").html('Select pickup station');
        $("#t_fare").text('0.0');
        return false;
    }
    else if($("#destination").val().length < 1)
    {
        showErrors('Select destination station');
        $("#t_destination").html('Select destination station');
        $("#t_fare").text('0.0');
        return false;
    }
    else if($("#destination").val() == $("#pickup").val())
    {
        showErrors('Pick up and destination station cannot be the same');
        $("#t_fare").text('0.0');
        return false;
    }
    else {
        return true;
    }
}
function addDestination() {
            if(valitaeForm()) {
                        $("#t_route").html($("#route option:selected").text());
                        $("#t_pickup").html($("#pickup option:selected").text());
                        $("#t_destination").html($("#destination option:selected").text());

                        var fareData = {};
                        fareData['from_id'] = $("#pickup").val();
                        fareData['to_id'] = $("#destination").val();
                        fareData['_token'] = '{{csrf_token()}}';
                        $.ajax({
                            url: '{{url('ticket/fare')}}',
                            type: 'POST',
                            data: fareData,
                            success: function (response) {
                                $("#t_fare").text(response['fare']);
                                if (response['status'] == 'FAIL') {
                                    $("#error").text('Insufficient account balance, pay with M-Pesa or Top Up Account').show();
                                }

                            },
                            error: function (response) {
                                $("#error").text('Sorry for this, seems like something went wrong. Try again later').show();
                            }
                        });
            }

        }


        function showErrors(msg) {
            swal(
                    'Error',
                    msg,
                    'error'
            );
        }

        function buyTicket() {
            var formData = {};
            formData['route'] = $("#route").val();
            formData['destination'] = $("#destination").val();
            formData['pickup'] = $("#pickup").val();
            formData['type'] = 'ajax';
            formData['fare'] = $("#t_fare").text();
            formData['reference'] = $("#t_ticket").text();
            formData['_token'] = '{{csrf_token()}}';
            $.ajax({
                url: '{{url('ticket/buy')}}',
                type: 'POST',
                data: formData,
                success: function (response) {
                    if(response['payment'] == 'failed')
                    {
                        if(response['error'] == 'insufficient')
                        {
                            $("#payment_btn").empty();
                            $("#payment_btn").append('<a href="{{url('transactions/create')}}" class="btn btn-sm btn-primary">Top up'+
                        '</a><button type="button" class="btn btn-xs btn-circle btn-stroke btn-warning">'+
                                    'OR</button><button type="button" data-target="#myModal" data-toggle="modal" class="btn btn-sm btn-success">'+
                                    'Pay with M-Pesa</button>');
                            showErrors('Failed: Insufficient account balance, pay with M-Pesa or Top Up Account');
                        }
                        else {
                            showErrors('Failed: The amount entered does not much our fare amount, try again');
                        }
                    }
                    else {
                        swal(
                                'Purchase successful',
                                'You bought ticket number ' + $("#t_ticket").html(),
                                'success'
                        ).then(function () {
                            window.location='{{url('/')}}';
                        });
                    }

                },
                error: function (response) {
                    if(response['responseJSON']['reference'])
                    {
                        showErrors('This ticket number, has been taken. Go to your My Ticket to confirm if you bought the ticket before buying again');
                    }
                    else if(response['responseJSON']['pickup'])
                    {
                        alert('Pick up station detail error, try again');
                        showErrors('Pick up station detail error, try again');
                    }
                    else if(response['responseJSON']['destination'])
                    {
                        alert('Destination station detail error, try again');
                        showErrors('Destination station detail error, try again');
                    }
                    else if(response['responseJSON']['fare'])
                    {
                        alert('Fare detail error, try again');
                        showErrors('Fare detail error, try again');
                    }
                    else {
                        showErrors('Sorry for this, seems like something went wrong. Try again later');
                    }
                }

            })
        }
        function addRoute(route_id,routes) {
            if(route_id.value.length < 1)
            {
                $("#pickup").empty().append('<option value="" hidden>Select pickup</option>');
                $("#destination").empty();
                $("#destination").append('<option value="" hidden>Select destination</option>');
            }
            else {
                $.each(routes, function (index, value) {
                    if (route_id.value == value['id']) {
                        $("#pickup").empty();
                        $("#destination").empty();
                        var data = '';
                        $.each(value['stages'], function (stage_index, stage_value) {
                            data = data + '<option value="' + stage_value['id'] + '">' +
                                    stage_value['name'] + '</option>';
                        })

                        $("#pickup").append('<option value="" hidden>Select pickup</option>'+ data);
                        $("#destination").append('<option value="" hidden>Select destination</option>'+ data);
                    }
                });
            }

            if(route_id.value.length > 0)
            {
                $("#t_route").html($("#route option:selected").text());
            }
            else {
                $("#t_route").html('Select route');
            }

            $("#t_pickup").html('Select pick up station');
            $("#t_destination").html('Select destination station');
            $("#t_fare").text('0.0');
        }

        function stationUpdate(valueX) {
            if(valueX == 'pickup' && $("#destination").val().length > 0)
            {
                addDestination();
            }
            if($('#'+valueX).val().length > 0)
            {
                $('#t_'+valueX).html($('#'+valueX+' option:selected').text());
            }
            else {
                $('#t_'+valueX).html('Select ' + valueX + ' station');
                $("#t_fare").text('0.0');
            }
        }

        function checkError() {
            if($("#route").val() == null)
            {
                alert("Select route");
                showErrors('Select route');
            }
        }
    </script>
@endsection