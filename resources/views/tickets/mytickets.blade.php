@extends('layout')
@section('content')
    <div class="container">
        <div class="page-section">
            <div class="row">

                <div class="col-md-9">

                    <div class="panel panel-default paper-shadow" data-z="0.5">
                        <div class="panel-heading">
                            <div class="max-width-300 form-group daterangepicker-report">
                                <div class="form-control">
                                    <i class="fa fa-calendar fa-fw"></i>
                                    <span>{{\Carbon\Carbon::now()->format('d-M-Y')}}</span>
                                    <b class="caret"></b>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table text-subhead v-middle">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Reference</th>
                                    <th>Route</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Fare</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($tickets as $ticket)
                                    <tr>
                                        <td><button class="btn btn-sm primary">{{\Carbon\Carbon::parse($ticket->created_at)->format('d-M-Y H:m')}}</button></td>
                                        <td class="text-uppercase">{{ucwords($ticket->reference)}}</td>
                                        <td>{{ucwords($ticket->userRoute->name)}} {{$ticket->userRoute->route}}</td>
                                        <td>{{$ticket->fromStation->name}}</td>
                                        <td>{{$ticket->toStation->name}}</td>
                                        <td class="text-right">{{$ticket->fare}}</td>
                                        <td>{{ucwords($ticket->status)}}</td>
                                        <td class="width-100">
                                            @if($ticket->status == \App\Ticket::ACTIVE)
                                                <a href="#" class="btn btn-sm  btn-primary"><i class="fa fa-pencil"></i></a>
                                            @endif
                                                <a href="" class="btn btn-sm btn-default">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="panel-footer">
                            Showing {{ $tickets->firstItem() }} to {{ $tickets->lastItem() }} of {{ $tickets->total() }} entries
                            <div class="pull-right">
                            {!! $tickets->links() !!}
                            </div>
                            {{--<ul class="pagination margin-none">--}}
                                {{--<li class="disabled"><a href="#">&laquo;</a></li>--}}
                                {{--<li class="active"><a href="#">{{ $tickets->lastItem() }} </a></li>--}}
                                {{--<li><a href="#">{{ $tickets->lastItem() }}  </a></li>--}}
                                {{--<li><a href="#">{!! $tickets->links() !!}</a></li>--}}
                                {{--<li><a href="#"></a></li>--}}
                            {{--</ul>--}}
                        </div>
                    </div>

                    <br/>
                    <br/>

                </div>
                <div class="col-md-3">

                    @include('partials.sidemenu')

                </div>

            </div>
        </div>
    </div>

@endsection