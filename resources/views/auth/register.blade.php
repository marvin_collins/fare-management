@extends('layout')

@section('content')
<div class="container">
    <div class="row" style="margin-top: 20px">
        @foreach($errors as $error)
            <p>{{$error}}</p>
        @endforeach
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading text-center"><h3>Register</h3></div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="col-md-12">
                            <div class="form-group form-control-material static required {{ $errors->has('name') ? ' has-error' : '' }}">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                <label for="name">Full Name</label>
                            </div>
                            <div class="form-group form-control-material static required {{ $errors->has('username') ? ' has-error' : '' }}">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required >
                                <label for="username">Username</label>
                            </div>
                            <div class="form-group form-control-material static required {{ $errors->has('national_id') ? ' has-error' : '' }}">
                                <input id="national_id" min="1" type="text" class="form-control" name="national_id" value="{{ old('national_id') }}" required >
                                <label for="national_id">National ID</label>
                            </div>
                            <div class="form-group form-control-material static required {{ $errors->has('phone') ? ' has-error' : '' }}">
                                <input id="phone" min="1" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required >
                                <label for="phone">Phone Number</label>
                            </div>
                            <div class="form-group form-control-material static required {{ $errors->has('email') ? ' has-error' : '' }}">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                <label for="email">Email Address</label>
                            </div>
                            <div class="form-group form-control-material static {{ $errors->has('password') ? ' has-error' : '' }} required">
                                <input id="password" type="password" class="form-control" name="password" required>
                                <label for="password">Password</label>
                            </div>
                            <div class="form-group form-control-material static required">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                <label for="password-confirm">Confrim Password</label>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn_space pull-right btn-primary">
                                    Register
                                </button>
                               <h4>Do you have an account?
                                <a href="{{url('/login')}}" >
                                    Login
                                </a></h4>
                            </div>
                        </div>
                        <div class="form-group">

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script src="/js/app.js"></script>
    @endsection
