@extends('layout')

@section('content')
    <div class="container">
        <div class="row" style="margin-top: 20px">
            @foreach($errors as $error)
                <p>{{$error}}</p>
            @endforeach
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><h3>Login</h3></div>
                    <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="col-md-10 col-md-offset-1">
                        <div class="form-group form-control-material static required {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            <label for="email">E-Mail Address
                            </label>
                        </div>

                        <div class="form-group form-control-material static required {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" type="password" class="form-control" name="password" required>
                            <label for="password" >Password</label>


                        </div>

                        <div class="checkbox checkbox-primary">
                            <input id="remember"  type="checkbox" name="remember" checked>
                            <label for="remember">
                                Remember Me
                            </label>
                        </div>


                        <div class="form-group">
                            {{--<div class="col-md-6 col-md-offset-3">--}}
                                <button type="submit" class="btn pull-right btn-primary">
                                    Login
                                </button>
                            {{--</div>--}}
                                <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        {{--</div>--}}
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="/js/app.js"></script>
@endsection
