
<!-- Fixed navbar -->
<div class="navbar navbar-default navbar-fixed-top navbar-size-large navbar-size-xlarge paper-shadow" data-z="0" data-animated role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-brand navbar-brand-logo">
                <a href="index.html">
                    <img  class="svg"  src="{{asset('images/logo.png')}}" width="80%" height="60%" style="margin-top: 3px" alt="logo">
                </a>
            </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="main-nav">
            <ul class="nav navbar-nav navbar-nav-margin-left">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us</a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('who-we-are')}}">Who we are</a></li>
                        <li><a href="{{asset(url('our-team'))}}">Our Team</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" >How It Works</a>
                </li>
                <li class="dropdown">
                    <a href="{{url('/ticket')}}" >Bus Ticket</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Monthly Plan <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        {{--<li class="active"><a href="website-student-dashboard.html">Daily plan</a></li>--}}
                        <li><a href="{{url('monthly-plan/create')}}">Monthly plan</a></li>
                        {{--<li><a href="website-course-forums.html">Monthly group plan</a></li>--}}
                        <li><a href="website-take-course.html">Monthly multiple routes plan</a></li>

                        {{--<li><a href="website-take-quiz.html">Take Quiz</a></li>--}}
                        {{--<li><a href="website-student-messages.html">Messages</a></li>--}}
                        {{--<li><a href="website-student-profile.html">Private Profile</a></li>--}}
                    </ul>
                </li>
                {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">FAQs</a>--}}
                {{--</li>--}}
                <li class="dropdown">
                    <a href="{{url('/contact')}}" >Contact Us</a>
                </li>
            </ul>
            <div class="navbar-right">
                @if (Auth::guest())
                    <a href="{{ url('/register') }}" class="navbar-btn btn btn-primary">Register</a>
                    <a href="{{ url('/login') }}" class="navbar-btn btn btn-success">Login</a>
                @else
                <ul class="nav navbar-nav navbar-nav-bordered navbar-nav-margin-right">
                    <!-- user -->
                    <li class="dropdown user active">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"  >
                            <img src="{{asset('images/people/110/guy-6.jpg')}}" alt="" class="img-circle" /> {{ Auth::user()->username }}<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li class="active"><a href="{{url('/')}}"><i class="fa fa-bar-chart-o"></i> My Account</a></li>
                            <li><a href="{{ url('/logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> Logout</a>
                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                    <!-- // END user -->
                </ul>
                    <a href="{{route('transactions.create')}}" class="navbar-btn btn-sm btn btn-primary">Top Account</a>
                @endif

            </div>
        </div>
        <!-- /.navbar-collapse -->

    </div>
</div>
