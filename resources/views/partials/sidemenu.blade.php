<div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
    <div class="panel-heading panel-collapse-trigger">
        <h4 class="text-headline margin-none">Quick Menu</h4>
    </div>
    <div class="panel-body list-group">
        <ul class="list-group list-group-menu">
            <li class="list-group-item "><a class="link-text-color" href="{{url('ticket/my-tickets')}}">My tickets</a></li>
            <li class="list-group-item "><a class="link-text-color" href="{{url('ticket')}}">Buy ticket</a></li>
            <li class="list-group-item"><a class="link-text-color" href="{{url('statements')}}">My account statement</a></li>
            <li class="list-group-item"><a class="link-text-color" href="{{url('payments')}}">My account payments</a></li>
            <li class="list-group-item"><a class="link-text-color" href="{{url('monthly-plan')}}">Monthly plans</a></li>
            <li class="list-group-item"><a class="link-text-color" href="website-instructor-earnings.html">Monthly plan: Multiple routes</a></li>
            {{--<li class="list-group-item"><a class="link-text-color" href="website-instructor-statement.html">Monthly plan: Group plan</a></li>--}}
            <li class="list-group-item"><a class="link-text-color" href="{{url('edit-account')}}">Edit Profile</a></li>
        </ul>
    </div>
</div>

<h4>Featured</h4>
<div class="slick-basic slick-slider" data-items="1" data-items-lg="1" data-items-md="1" data-items-sm="1" data-items-xs="1">

    <div class="item">
        <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
            <div class="panel-body">
                <div class="media media-clearfix-xs">
                    <div class="media-left">
                        <div class="cover width-90 width-100pc-xs overlay cover-image-full hover">
                            <span class="img icon-block s90 bg-lightred"></span>
                            <span class="overlay overlay-full padding-none icon-block s90 bg-lightred">
                        <span class="v-center">
                            <i class="fa fa-windows"></i>
                        </span>
                        </span>
                            <a href="website-course.html" class="overlay overlay-full overlay-hover overlay-bg-white">
                          <span class="v-center">
                            <span class="btn btn-circle btn-red-500 btn-lg"><i class="fa fa-graduation-cap"></i></span>
                          </span>
                            </a>
                        </div>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading margin-v-5-3"><a href="website-course.html">Portable Environments with Vagrant</a></h4>
                        <p class="small margin-none">
                            <span class="fa fa-fw fa-star text-yellow-800"></span>
                            <span class="fa fa-fw fa-star text-yellow-800"></span>
                            <span class="fa fa-fw fa-star text-yellow-800"></span>
                            <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                            <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item">
        <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
            <div class="panel-body">
                <div class="media media-clearfix-xs">
                    <div class="media-left">
                        <div class="cover width-90 width-100pc-xs overlay cover-image-full hover">
                            <span class="img icon-block s90 bg-brown"></span>
                            <span class="overlay overlay-full padding-none icon-block s90 bg-brown">
                        <span class="v-center">
                            <i class="fa fa-wordpress"></i>
                        </span>
                        </span>
                            <a href="website-course.html" class="overlay overlay-full overlay-hover overlay-bg-white">
                          <span class="v-center">
                            <span class="btn btn-circle btn-orange-500 btn-lg"><i class="fa fa-graduation-cap"></i></span>
                          </span>
                            </a>
                        </div>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading margin-v-5-3"><a href="website-course.html">WordPress Theme Development</a></h4>
                        <p class="small margin-none">
                            <span class="fa fa-fw fa-star text-yellow-800"></span>
                            <span class="fa fa-fw fa-star text-yellow-800"></span>
                            <span class="fa fa-fw fa-star text-yellow-800"></span>
                            <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                            <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item">
        <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
            <div class="panel-body">
                <div class="media media-clearfix-xs">
                    <div class="media-left">
                        <div class="cover width-90 width-100pc-xs overlay cover-image-full hover">
                            <span class="img icon-block s90 bg-purple"></span>
                            <span class="overlay overlay-full padding-none icon-block s90 bg-purple">
                        <span class="v-center">
                            <i class="fa fa-jsfiddle"></i>
                        </span>
                        </span>
                            <a href="website-course.html" class="overlay overlay-full overlay-hover overlay-bg-white">
                          <span class="v-center">
                            <span class="btn btn-circle btn-purple-500 btn-lg"><i class="fa fa-graduation-cap"></i></span>
                          </span>
                            </a>
                        </div>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading margin-v-5-3"><a href="website-course.html">Modular JavaScript with Browserify</a></h4>
                        <p class="small margin-none">
                            <span class="fa fa-fw fa-star text-yellow-800"></span>
                            <span class="fa fa-fw fa-star text-yellow-800"></span>
                            <span class="fa fa-fw fa-star text-yellow-800"></span>
                            <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                            <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item">
        <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
            <div class="panel-body">
                <div class="media media-clearfix-xs">
                    <div class="media-left">
                        <div class="cover width-90 width-100pc-xs overlay cover-image-full hover">
                            <span class="img icon-block s90 bg-default"></span>
                            <span class="overlay overlay-full padding-none icon-block s90 bg-default">
                        <span class="v-center">
                            <i class="fa fa-cc-visa"></i>
                        </span>
                        </span>
                            <a href="website-course.html" class="overlay overlay-full overlay-hover overlay-bg-white">
                          <span class="v-center">
                            <span class="btn btn-circle btn-white btn-lg"><i class="fa fa-graduation-cap"></i></span>
                          </span>
                            </a>
                        </div>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading margin-v-5-3"><a href="website-course.html">Easy Online Payments with Stripe</a></h4>
                        <p class="small margin-none">
                            <span class="fa fa-fw fa-star text-yellow-800"></span>
                            <span class="fa fa-fw fa-star text-yellow-800"></span>
                            <span class="fa fa-fw fa-star text-yellow-800"></span>
                            <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                            <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

