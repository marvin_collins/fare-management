<div class="parallax overflow-hidden bg-blue-400 page-section third">
    <div class="container parallax-layer" data-opacity="true">
        <div class="row">
            <div class="col-sm-12">
                <div class="media v-middle">
                    <div class="media-left text-center">
                        <a href="#">
                            <img src="images/people/110/guy-6.jpg" alt="people" class="img-circle width-80" />
                        </a>
                    </div>
                    <div class="media-body">
                        <h2 class="text-white text-display-1 margin-v-0">{{\Illuminate\Support\Facades\Auth::user()->name}}</h2>
                        <p class="text-subhead"><a class="link-white text-underline" href="website-student-public-profile.html"></a></p>
                    </div>
                    <div class="media-left">
                        <span class="label bg-blue-500">Current balance: Ksh  {{\App\Wallet::where('user_id',Auth::user()->id)->first()->balance}}</span>
                    </div>
                </div>
            </div>
            {{--<div class="col-sm-4">--}}
                {{--<div class="media v-middle">--}}
                    {{--<div class="media-left text-center">--}}
                        {{--<a href="#">--}}
                            {{--<img src="images/people/110/guy-6.jpg" alt="people" class="img-circle width-80" />--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="media-body">--}}
                        {{--<h1 class="text-white text-display-1 margin-v-0">Bill Smith</h1>--}}
                        {{--<p class="text-subhead"><a class="link-white text-underline" href="website-student-public-profile.html">View public profile</a></p>--}}
                    {{--</div>--}}
                    {{--<div class="media-right">--}}
                        {{--<span class="label bg-blue-500">Student</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</div>