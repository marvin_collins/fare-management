<div class="sidebar left sidebar-size-3 sidebar-offset-0 sidebar-visible-desktop sidebar-visible-mobile sidebar-skin-dark" id="sidebar-menu" data-type="collapse">
    <div data-scrollable>
        <div class="sidebar-block">
            <div class="profile">
                <a href="#">
                    <img src="{{asset('images/people/110/guy-6.jpg')}}" alt="people" class="img-circle width-80" />
                </a>
                <h4 class="text-display-1 margin-none">Instructor Name</h4>
            </div>
        </div>

        <ul class="sidebar-menu">
            <li class="hasSubmenu">
                <a href="#forum-menu"><i class="fa fa-bus"></i><span>Bus Operators</span></a>
                <ul id="forum-menu">
                    <li><a href="{{route('bus-operators.create')}}">
                            <span>Add Bus Operators</span>
                        </a>
                    </li>
                    <li><a href="app-forum-category.html">
                            <span>View All Buses</span>
                        </a>
                    </li>
                    <li><a href="app-forum-thread.html">
                            <span>Blacklisted Operators</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
