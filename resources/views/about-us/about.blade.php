@extends('layout')
@section('content')
    <div class="content" >
        <div class="parallax bg-blue-300 page-section">
            <div class="parallax-layer container" data-opacity="true">
                <div class="media v-middle">

                        <div class="col-sm-12 col-md-8">
                            <div class="col-md-2 col-sm-4">
                                <span class="icon-block s60 bg-lightred" style="background: #42a5f5 !important;"><i class="fa fa-building"></i></span>
                            </div>
                            <div class="col-md-10 col-sm-8 pull-left">
                                <h1 class="text-display-1 margin-none" style="color: white">
                                    Who We Are</h1>

                        <p class="small margin-none">
                            <span style="color: white" class="fa fa-fw fa-car fa-2x"></span>
                            <span style="color: white" class="fa fa-fw fa-taxi fa-2x"></span>
                            <span style="color: white" class="fa fa-fw fa-cab fa-2x"></span>
                            <span style="color: white" class="fa fa-fw fa-truck fa-2x"></span>
                            <span style="color: white" class="fa fa-fw fa-bus  fa-2x"></span>
                            <span style="color: white" class="fa fa-fw fa-road fa-2x" ></span>
                        </p>
                            </div>
                    </div>

                        <div class="col-sm-12 col-md-4">
                        <a class="btn btn-white" href="{{url('our-team')}}">Our Team</a>
                        <a class="btn btn-white" href="{{url('contact')}}">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2" style="margin-bottom: 30px">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="page-section">
                        <div class="width-350 width-300-md width-100pc-xs paragraph-inline">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="//www.youtube-nocookie.com/embed/Ycv5fNd4AeM?rel=0"></iframe>
                            </div>
                        </div>
                        <p>
                            <strong>Lorem ipsum</strong> dolor sit amet, consectetur adipisicing elit. Ad aperiam autem cumque deleniti dicta iusto laboriosam laudantium omnis, possimus praesentium provident quam quas, sapiente sint, ut! Adipisci aliquid assumenda consequuntur
                            cupiditate deleniti dicta dolore dolorem
                            <strong>dolores enim </strong>eos hic illo inventore iure libero magnam minima minus obcaecati optio pariatur porro quibusdam quos reiciendis, sapiente sint veritatis. Eveniet in magni sunt?</p>
                        <p>
                            <strong>Lorem ipsum</strong> dolor sit amet, consectetur adipisicing elit. Ad aperiam autem cumque deleniti dicta iusto laboriosam laudantium omnis, possimus praesentium provident quam quas, sapiente sint, ut! Adipisci aliquid assumenda consequuntur
                            cupiditate deleniti dicta dolore dolorem
                            <strong>dolores enim </strong>eos hic illo inventore iure libero.
                        </p>
                        <br/>
                        <p>
                            <strong>Lorem ipsum</strong> dolor sit amet, consectetur adipisicing elit. Ad aperiam autem cumque deleniti dicta iusto laboriosam laudantium omnis, possimus praesentium provident quam quas, sapiente sint, ut! Adipisci aliquid assumenda consequuntur
                            cupiditate deleniti dicta dolore dolorem
                            <strong>dolores enim </strong>eos hic illo inventore iure libero magnam minima minus obcaecati optio pariatur porro quibusdam quos reiciendis, sapiente sint veritatis. Eveniet in magni sunt?</p>
                        <br/>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @include('partials.footer')
@endsection