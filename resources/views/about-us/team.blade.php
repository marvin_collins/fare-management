@extends('layout')
@include('partials.topbarmenu')
@section('content')
    <div class="content">
        <div class="parallax bg-blue-300 page-section">
            <div class="parallax-layer container" data-opacity="true">
                <div class="col-sm-12 col-md-8">
                    <div class="col-md-2 col-sm-4">
                        <span class="icon-block s60 bg-lightred"
                              style="background: #42a5f5 !important;"><i class="fa fa-group"></i></span>
                    </div>
                    <div class="col-md-10 col-sm-8 pull-left">
                        <h1 class="text-display-1 margin-none" style="color: white">Our Team</h1>
                        <p class="small margin-none">
                            <span style="color: white" class="fa fa-fw fa-car fa-2x"></span>
                            <span style="color: white" class="fa fa-fw fa-taxi fa-2x"></span>
                            <span style="color: white" class="fa fa-fw fa-cab fa-2x"></span>
                            <span style="color: white" class="fa fa-fw fa-truck fa-2x"></span>
                            <span style="color: white" class="fa fa-fw fa-bus  fa-2x"></span>
                            <span style="color: white" class="fa fa-fw fa-road fa-2x" ></span>
                        </p>
                    </div>
                    </div>
                <div class="col-sm-12 col-md-4">
                        <a class="btn btn-white" href="{{asset(url('/who-we-are'))}}">Who We Are</a>

                        <a class="btn btn-white" href="{{url('contact')}}">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">

        <div class="page-section">
            <div class="row">

                <div class="col-md-10 col-md-offset-1">

                    <div class="row" data-toggle="isotope">
                        <div class="item col-xs-12 col-sm-6 col-lg-4">
                            <div class="panel panel-default paper-shadow" data-z="0.5">

                                <div class="cover overlay cover-image-full hover">
                                    <span class="img icon-block height-150 bg-primary">

                                    </span>
                                    <a href="#" class="padding-none overlay overlay-full icon-block bg-primary">
                                         <span class="v-center">
                                           <img src="{{asset('images/people/110/guy-6.jpg')}}">
                                        </span>
                                    </a>
                                    <span class="v-center">
                            <span class="btn btn-circle btn-primary btn-lg">
                                <i class="fa fa-user"></i>
                            </span>
                    </span>

                                </div>

                                <div class="panel-body">
                                    <h4 class="text-headline text-center margin-v-0-10">
                                        Gloria Jepkemboi
                                    </h4>

                                </div>
                                <hr class="margin-none" />
                                <div class="panel-body">
                                    <p>
                                        Managing Director
                                    </p>
                                    <div class="text-center">
                                        <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">
                                            <i class="fa fa-fw fa-twitter"></i></a>
                                        <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">
                                            <i class="fa fa-fw fa-facebook"></i></a>
                                        <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">
                                            <i class="fa fa-fw fa-linkedin"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="item col-xs-12 col-sm-6 col-lg-4">
                            <div class="panel panel-default paper-shadow" data-z="0.5">

                                <div class="cover overlay cover-image-full hover">
                                    <span class="img icon-block height-150 bg-primary">

                                    </span>
                                    <a href="#" class="padding-none overlay overlay-full icon-block bg-primary">
                                         <span class="v-center">
                                           <img src="{{asset('images/people/110/guy-6.jpg')}}">
                                        </span>
                                    </a>
                                    <span class="v-center">
                            <span class="btn btn-circle btn-primary btn-lg">
                                <i class="fa fa-user"></i>
                            </span>
                    </span>

                                </div>

                                <div class="panel-body">
                                    <h4 class="text-headline text-center margin-v-0-10">
                                        Evans Wanguba
                                    </h4>

                                </div>
                                <hr class="margin-none" />
                                <div class="panel-body">
                                    <p>
                                        Marketing Director
                                    </p>
                                    <div class="text-center">
                                        <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">
                                            <i class="fa fa-fw fa-twitter"></i></a>
                                        <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">
                                            <i class="fa fa-fw fa-facebook"></i></a>
                                        <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">
                                            <i class="fa fa-fw fa-linkedin"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="item col-xs-12 col-sm-6 col-lg-4">
                            <div class="panel panel-default paper-shadow" data-z="0.5">

                                <div class="cover overlay cover-image-full hover">
                                    <span class="img icon-block height-150 bg-primary"></span>
                                    <a href="#" class="padding-none overlay overlay-full icon-block bg-primary">
                    <span class="v-center">
                <img src="{{asset('images/people/110/guy-6.jpg')}}">
            </span>
                                    </a>
                                </div>

                                <div class="panel-body">
                                    <h4 class="text-headline text-center margin-v-0-10">
                                            Collins Marvin
                                    </h4>
                                </div>
                                <hr class="margin-none" />
                                <div class="panel-body">
                                    <p>
                                        IT Lead
                                    </p>
                                    <div class="text-center">
                                    <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">
                                        <i class="fa fa-fw fa-twitter"></i></a>
                                    <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">
                                        <i class="fa fa-fw fa-facebook"></i></a>
                                    <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">
                                        <i class="fa fa-fw fa-linkedin"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item col-xs-12 col-sm-6 col-lg-4">
                            <div class="panel panel-default paper-shadow" data-z="0.5">

                                <div class="cover overlay cover-image-full hover">
                                    <span class="img icon-block height-150 bg-primary"></span>
                                    <a href="#" class="padding-none overlay overlay-full icon-block bg-primary">
                    <span class="v-center">
                <img src="{{asset('images/people/110/guy-6.jpg')}}">
            </span>
                                    </a>
                                </div>

                                <div class="panel-body">
                                    <h4 class="text-headline text-center margin-v-0-10">
                                            Collins Marvin
                                    </h4>
                                </div>
                                <hr class="margin-none" />
                                <div class="panel-body">
                                    <p>
                                        IT Lead
                                    </p>
                                    <div class="text-center">
                                    <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">
                                        <i class="fa fa-fw fa-twitter"></i></a>
                                    <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">
                                        <i class="fa fa-fw fa-facebook"></i></a>
                                    <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">
                                        <i class="fa fa-fw fa-linkedin"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item col-xs-12 col-sm-6 col-lg-4">
                            <div class="panel panel-default paper-shadow" data-z="0.5">

                                <div class="cover overlay cover-image-full hover">
                                    <span class="img icon-block height-150 bg-primary"></span>
                                    <a href="#" class="padding-none overlay overlay-full icon-block bg-primary">
                    <span class="v-center">
                <img src="{{asset('images/people/110/guy-6.jpg')}}">
            </span>
                                    </a>
                                </div>

                                <div class="panel-body">
                                    <h4 class="text-headline text-center margin-v-0-10">
                                            Collins Marvin
                                    </h4>
                                </div>
                                <hr class="margin-none" />
                                <div class="panel-body">
                                    <p>
                                        IT Lead
                                    </p>
                                    <div class="text-center">
                                    <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">
                                        <i class="fa fa-fw fa-twitter"></i></a>
                                    <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">
                                        <i class="fa fa-fw fa-facebook"></i></a>
                                    <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">
                                        <i class="fa fa-fw fa-linkedin"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item col-xs-12 col-sm-6 col-lg-4">
                            <div class="panel panel-default paper-shadow" data-z="0.5">

                                <div class="cover overlay cover-image-full hover">
                                    <span class="img icon-block height-150 bg-primary"></span>
                                    <a href="#" class="padding-none overlay overlay-full icon-block bg-primary">
                    <span class="v-center">
                <img src="{{asset('images/people/110/guy-6.jpg')}}">
            </span>
                                    </a>
                                </div>

                                <div class="panel-body">
                                    <h4 class="text-headline text-center margin-v-0-10">
                                            Collins Marvin
                                    </h4>
                                </div>
                                <hr class="margin-none" />
                                <div class="panel-body">
                                    <p>
                                        IT Lead
                                    </p>
                                    <div class="text-center">
                                    <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">
                                        <i class="fa fa-fw fa-twitter"></i></a>
                                    <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">
                                        <i class="fa fa-fw fa-facebook"></i></a>
                                    <a class="btn btn-white btn-flat paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#">
                                        <i class="fa fa-fw fa-linkedin"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>

            </div>
        </div>

    </div>
    </div>
    @include('partials.footer')
@endsection