@extends('layout')
@section('content')
    <div class="container">
        <div class="row" style="margin-top: 20px">
            @foreach($errors as $error)
                <p>{{$error}}</p>
            @endforeach
            <div class="col-md-10 col-sm-12 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        <h3><i class="fa fa-calendar"></i> Monthly Plan Subscription</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="POST" id="buy_ticket">
                            {{ csrf_field() }}
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-sm-6 pull-right">
                                        <div id="account_balance" class="well well-sm text-center well-dafault" >
                                            Account Balance: Ksh: {{$wallet->balance}}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label style="margin-left: -10px">Select plan start date</label>
                                    </div>
                                    <div style="margin-left: -10px !important;" class="col-md-6 required">
                                        <div style="margin-bottom: 10px" class="input-group required">
                                            <div class="input-group-addon" style="color: inherit;">Start Date</div>
                                            <input required type="text" class="form-control date required" id="datetimepicker1" >
                                            <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar">
                                            </span>
                                              </span>
                                        </div>
                                    </div>
                                    <div style="margin-right: -10px !important;" class="col-md-6 pull-right">
                                        <div style="margin-bottom: 10px" class="input-group required">
                                            <div class="input-group-addon" style="color: inherit;">End Date</div>
                                            <input type="text" disabled class="form-control date" id="datetimepicker2" >
                                            <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar">
                                            </span>
                                              </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group form-control-material static required">
                                        <select name="monthly_plan_type" onchange="stationUpdate('monthly_plan_type')" id="monthly_plan_type" class="form-control" data-toggle="select2"
                                                data-style="btn-white" data-live-search="true" data-size="5">
                                            <option value="" >Select Monthly Plan </option>
                                            @foreach(\App\Plan::all() as $plan)
                                                <option value="{{$plan->id}}">{{ucwords($plan->name)}}</option>
                                            @endforeach
                                        </select>
                                        <label for="monthly_plan_type">Select Monthly Plan</label>
                                    </div>
                                    <div class="form-group form-control-material static required">
                                        <select name="plan_type" onchange="stationUpdate('plan_type')" id="plan_type" class="form-control" data-toggle="select2"
                                                data-style="btn-white" data-live-search="true" data-size="5">
                                            <option value="" >Select Plan Type</option>
                                            <option value="{{\App\MonthlyPlan::PLAN_TYPE_ONEWAY}}">
                                                {{ucwords(\App\MonthlyPlan::PLAN_TYPE_ONEWAY)}}
                                            </option>
                                            <option value="{{\App\MonthlyPlan::PLAN_TYPE_RETURN}}">
                                                {{ucwords(\App\MonthlyPlan::PLAN_TYPE_RETURN)}}
                                            </option>
                                        </select>
                                        <label for="plan_type">Select Plan Type</label>
                                    </div>
                                    <div class="form-group form-control-material static required">
                                        <input type="number" min="1" onchange="passangerChange()" onkeyup="this.onchange();"  required value="1" class="form-control" id="passangers" name="passangers">
                                        <label for="passangers">Number of adults</label>
                                    </div>
                                    <div class="form-group form-control-material static required">
                                            <select onchange="addRoute(this,{{$routes}})" name="route" id="route" class="form-control inputs" data-toggle="select2"
                                                    data-style="btn-white" data-live-search="true" data-size="5">
                                                <option value="" >Select route</option>
                                                @foreach($routes as $rte)
                                                     <option value="{{$rte->id}}">{{$rte->route}}</option>
                                                @endforeach
                                            </select>
                                        <label for="route">Route</label>
                                    </div>
                                    <div class="form-group form-control-material static required">
                                            <select name="pickup" onchange="stationUpdate('pickup')" id="pickup" class="form-control inputs" data-toggle="select2"
                                                    data-style="btn-white" data-live-search="true" data-size="5">
                                                <option value="" >Select pickup</option>
                                            </select>
                                        <label for="route">Pick Up Stage</label>
                                    </div>
                                    <div class="form-group form-control-material static required">
                                        <select name="destination" id="destination" onchange="addDestination()" class="form-control inputs" data-toggle="select2"
                                                data-style="btn-white" data-live-search="true" data-size="5">
                                            <option value="" >Select destination</option>
                                        </select>
                                    <label for="route">Destination</label>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading"><h4 class="text-center" style="color: inherit">
                                            Plan Summary</h4></div>
                                    <div class="panel-body">
                                        <table class="table table-stripped table-condensed">
                                            <tr>
                                                <th>Start Date</th>
                                                <td id="t_start_date">{{\Carbon\Carbon::now()->format('m/d/Y g:m:s A')}}</td>
                                            </tr>
                                            <tr>
                                                <th>End Date</th>
                                                <td id="t_end_date">{{\Carbon\Carbon::now()->addDays(30)->format('m/d/Y g:m:s A')}}</td>
                                            </tr>
                                            <tr>
                                                <th>Monthly plan</th>
                                                <td id="t_monthly_plan_type">Select monthly plan</td>
                                            </tr>
                                            <tr>
                                                <th>Plan type</th>
                                                <td id="t_plan_type">Select plan type</td>
                                            </tr>
                                            <tr>
                                                <th>Route</th>
                                                <td id="t_route">Select route</td>
                                            </tr>
                                            <tr>
                                                <th>Pick-up</th>
                                                <td id="t_pickup">Select pick up station</td>
                                            </tr>
                                            <tr>
                                                <th>Destination</th>
                                                <td id="t_destination">Select destination station</td>
                                            </tr>
                                            <tr>
                                                <th>Plan No:</th>
                                                <td id="t_ticket">{{$ticket}}</td>
                                            </tr>
                                            <tr>
                                                <th>Travellers</th>
                                                <td id="t_passangers">1</td>
                                            </tr>
                                            <tr>
                                                <th>Total cost</th>
                                                <td >Ksh: <span id="t_fare">0.0</span></td>
                                            </tr>
                                            <tr>
                                                <th>Apply Coupon</th>
                                                <td >
                                                    <div class="form-group form-control-material static">
                                                        <input id="coupon" type="text" placeholder="Coupon" class="form-control text-uppercase" name="coupon" >
                                                        <label for="coupon">Coupon
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">

                            </div>
                            <div class="row">
                                <h3 class="text-center"><i class="fa fa-suitcase">

                                    </i> Traveller Details</h3>

                                <table class="table table-stripped">
                                    <tbody id="addTraveller">
                                    <tr id="number1" class="travellers_d">
                                        <td>Traveller 1 </td>
                                        <td>
                                            <div class="form-group form-control-material static required ">
                                                <input id="tname" type="text" placeholder="Full name" class="travellers form-control" value="{{$user->name}}" name="[traveller1[name][]]" required>
                                                <label >Full Name
                                                </label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group form-control-material static required ">
                                                <input id="tphone" type="number" min="1" placeholder="Phone number" value="{{$user->phone}}" class="travellers form-control" name="[traveller1[phone][]]" required>
                                                <label >Phone number
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="col-md-6 col-xs-12">

                                </div>
                            </div>

                            <div class="col-md-12">
                                    <pre hidden id="error" style="color: red; margin-left: -10px !important;">Your account balance is insufficient</pre>
                                <div class="row">
                                    <div style="margin-left: -10px !important;" class="col-sm-4">
                                        <a href="{{url()->previous()}}" class="btn btn-circle btn-stroke btn-primary">
                                            <i class="fa fa-fw fa-arrow-left"></i>
                                        </a>
                                    </div>
                                    <div id="payment_btn" class="pull-right col-sm-8 text-center">
                                        @if($wallet->balance > 20)
                                            <button type="submit" onclick="checkError()" class="pull-right btn btn-primary">
                                                Subscribe
                                            </button>
                                        @else
                                            <a href="{{url('transactions/create')}}" class="pull-right btn btn-xs btn-primary">
                                                Top up
                                            </a>

                                            <button type="button" class="pull-right btn btn-xs btn-circle btn-stroke btn-warning">
                                                OR</button>
                                            <button type="button" data-target="#myModal" data-toggle="modal" class="pull-right btn btn-xs pull-right btn-success">
                                                Pay with M-Pesa
                                            </button>

                                        @endif
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <form class="form-horizontal" role="form" id="mpesaform" >
                <div class="modal-content">
                    <div class="modal-header btn-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center" id="myModalLabel"h4 style="color: inherit">Pay with M-Pesa</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h4>Instructions</h4>
                                <ol>
                                    <li>Go to M-PESA Menu</li>
                                    <li>Select Pay Bill</li>
                                    <li>Enter 552552 as the Business Number</li>
                                    <li>Enter 123456 as the Account Number</li>
                                    <li>Enter the value amount to pay eg 3000 (NO COMMAS)</li>
                                    <li>Enter your M-PESA PIN</li>
                                    <li>Then send the request</li>
                                    <li>You will receive an SMS confirming the transaction</li>
                                    <li>Enter the M-PESA REFERENCE NUMBER below</li>
                                    <li>Click finish button below to finish</li>
                                </ol>
                            </div>
                        </div>
                            <div class="col-md-12">
                                <div class="form-group form-control-material static required {{ $errors->has('topedamount') ? ' has-error' : '' }}">
                                    <input id="topedamount" type="number" min="20" class="form-control" name="topedamount" value="{{ old('topedamount') }}" required autofocus>
                                    <label for="topedamount">Amount
                                    </label>
                                </div>
                                <div class="form-group form-control-material static required {{ $errors->has('reference') ? ' has-error' : '' }}">
                                    <input id="reference" type="text" placeholder="Enter M-Pesa Reference" class="form-control text-uppercase" name="reference" value="{{ old('reference') }}" required>
                                    <label for="reference">M-PESA REFERENCCE NUMBER
                                    </label>
                                </div>
                            </div>
                        <button type="button" id="dismiss" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn pull-right btn-sm btn-primary btn-primary">Finish</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@include('partials.footer')
@endsection
@section('scripts')
    <script>
        function changeDate() {
            var ndate = new Date($("#datetimepicker1").val());
            var date = new Date(ndate);
            date.setDate(date.getDate() + 30);
            var dd = date.getDate();
            var mm = date.getMonth()+1;
            var yy = date.getFullYear();
            var hh = date.getHours();
            var mms = date.getMinutes();
            var ampm = hh >= 12 ? 'PM' : 'AM';
            hh = hh % 12;
            hh = hh ? hh : 12;

            $('#datetimepicker2').val('0' + mm + '/' + (dd > 9 ? dd : '0' + dd ) + '/' + yy + ' ' + hh + ':' + (mms > 9 ? mms : '0' + mms ) + ' ' + ampm);
            $("#t_start_date").html($("#datetimepicker1").val());
            $("#t_end_date").html('0' + mm + '/' + (dd > 9 ? dd : '0' + dd )  + '/' + yy + ' ' + hh + ':' + (mms > 9 ? mms : '0' + mms ) + ' ' + ampm);
        }
        $(function () {
                    $('#datetimepicker1').datetimepicker(
                    {
                        defaultDate: '{{\Carbon\Carbon::now()->format('m/d/Y g:m:s A')}}',
                        minDate: '{{\Carbon\Carbon::now()->format('m/d/Y g:m:s A')}}',
                        viewMode: 'days'
                    });

            $('#datetimepicker2').datetimepicker(
                    {
                        defaultDate: '{{\Carbon\Carbon::now()->addDays(30)->format('m/d/Y g:m:s A')}}',
                    });

                    $('#datetimepicker1').on('dp.change', function (e) {
                       changeDate();
                    });
                });
//        $("#error").hide();
function validateForm() {
    if($("#monthly_plan_type").val().length < 1)
    {
        showErrors('Select monthly plan');
        return false;
    }
    else if($("#plan_type").val().length < 1)
    {
        showErrors('Select plan type');
        return false;
    }
    else if($("#route").val().length < 1)
    {
        showErrors('Select route');
        return false;
    }
    else if($("#pickup").val().length < 1)
    {
        showErrors('Select pickup station');
        return false;
    }
    else if($("#destination").val().length < 1)
    {
        showErrors('Select destination station');
        return false;
    }
    else if($("#destination").val() == $("#pickup").val())
    {
        showErrors('Pick up and destination station cannot be the same');
        return false;
    }
    else {
        return true;
    }
}

$('#mpesaform').on('submit', function (e) {
    e.preventDefault();
    var mpesaData = {};
    mpesaData['reference'] = $('#reference').val();
    mpesaData['ajax'] = 'ajax';
    mpesaData['topedamount'] = $('#topedamount').val();
    mpesaData['amount'] = $('#t_fare').text();
    mpesaData['_token'] = '{{csrf_token()}}';
    $.ajax({
        url: '{{route('transactions.store')}}',
        type: 'POST',
        data: mpesaData,
        success: function (response) {
            if(response['message'] == 'success')
            {
                $('#account_balance').html('Account Balance: Ksh: ' + response['amount']);
                $("#payment_btn").empty();
                $("#payment_btn").append('<button type="submit" onclick="checkError()" class="pull-right btn btn-primary">'+
                'Subscribe</button>');
                $('#error').hide();
            }
            else {
                showErrors('Your account balance is still insufficient, the amount you sent was not enough');
            }
        },
        error: function (response) {
            if(response['responseJSON']['reference'])
            {
                showErrors('The M_Pesa Reference Number you entered has been used before, please try again');
            }
        }
    })
});

$('#buy_ticket').on('submit', function (e) {
            e.preventDefault();
//    $.each($('.travellers'),function (index,values) {
//        console.log($(values).val());
//    });
            if(validateForm()) {
                buyTicket();
            }
        });

        function addDestination() {
           if(validateForm()) {
               if ($("#destination").val().length > 0) {
                   if ($("#destination").val() != $("#pickup").val()) {
                       $("#t_route").html($("#route option:selected").text());
                       $("#t_pickup").html($("#pickup option:selected").text());
                       $("#t_destination").html($("#destination option:selected").text());

                       var fareData = {};
                       fareData['from_id'] = $("#pickup").val();
                       fareData['plan_type'] = $("#plan_type").val();
                       fareData['passangers'] = $("#passangers").val();
                       fareData['monthly_plan_type_id'] = $("#monthly_plan_type").val();
                       fareData['to_id'] = $("#destination").val();
                       fareData['_token'] = '{{csrf_token()}}';
                       $.ajax({
                           url: '{{url('monthly-plan/cost')}}',
                           type: 'POST',
                           data: fareData,
                           success: function (response) {
                               $("#t_fare").text(response['fare']);
                               if (response['status'] == 'FAIL') {

                                   $("#error").text('Insufficient account balance, pay with M-Pesa or Top Up Account').show();

                                   $("#payment_btn").empty();

                                   $("#payment_btn").append('<a href="{{url('transactions/create')}}" class="pull-right btn btn-sm btn-primary">Top up'+
                                           '</a><button type="button" class="pull-right btn btn-xs btn-circle btn-stroke btn-warning">'+
                                           'OR</button><button type="button" data-target="#myModal" data-toggle="modal" class="pull-right btn btn-sm btn-success">'+
                                           'Pay with M-Pesa</button>');
                               }
                               else{
                                   $("#payment_btn").empty();
                                   $("#payment_btn").append('<button type="submit" onclick="checkError()" class="pull-right btn btn-primary">'+
                                           'Subscribe</button>');
                                   $('#error').hide();
                               }

                           },
                           error: function (response) {
                               showErrors('Sorry for this, seems like something went wrong. Try again later').show();
                           }
                       })
                   }
                   else {
                       showErrors('Pick up and destination station cannot be the same');
                   }
               }
               else {
                   $("#t_destination").html('Select destination station');
                   $("#t_fare").text('0.0');
               }
           }
        }

        function showErrors(msg) {
            swal(
                    'Error',
                    msg,
                    'error'
            );
        }

        function passangerChange() {

            var travellers = $("#passangers").val();
            var passangers = $('.travellers_d').length;

            $("#t_passangers").html(travellers);
            if(travellers > 1){
                for (var i = 1; i < travellers; i++)
                {
                    if( passangers > travellers){
                        for(var ext = travellers; ext < passangers; ext++ ){
                            $('#number'+ext+1).empty();
                        }
                     }


                     if(passangers != travellers) {
                         var data = '<tr id="number' + (passangers + i) + '" class="travellers_d"><td>Traveller ' + (1 + i) + ' </td><td>' +
                                 '<div class="form-group form-control-material static required "><input ' +
                                 'id="tname" type="text" placeholder="Full name" class="travellers form-control"  ' +
                                 'name="[traveller1[name][]]" required><label >Full Name</label></div></td><td>' +
                                 '<div class="form-group form-control-material static required "><input id="tphone" type="number" min="1" placeholder="Phone number"' +
                                 'class="travellers form-control" name="[traveller1[phone][]]" required><label >Phone number</label>' +
                                 '</div></td></tr>';

                         $('#addTraveller').append(data);
                     }

                }
            }

            if($("#destination").val().length > 0)
            {
                addDestination();
            }
        }

        function buyTicket() {
            var formData = {};

            formData['route_id'] = $("#route").val();
            formData['monthly_plan_type_id'] = $("#monthly_plan_type").val();
            formData['plan_type'] = $("#plan_type").val();
            formData['passangers'] = $("#passangers").val();
            formData['plan_end_date'] = $("#datetimepicker2").val();
            formData['plan_start_date'] = $("#datetimepicker1").val();
            formData['terminal_two'] = $("#destination").val();
            formData['terminal_one'] = $("#pickup").val();
            formData['type'] = 'ajax';
            formData['amount'] = $("#t_fare").text();
            formData['reference'] = $("#t_ticket").text();
            formData['_token'] = '{{csrf_token()}}';
            $.ajax({
                url: '{{url('monthly-plan/subscription')}}',
                type: 'POST',
                data: formData,
                success: function (response) {
                    if(response['payment'] == 'failed')
                    {
                        if(response['error'] == 'insufficient')
                        {
                            $("#payment_btn").empty();
                            $("#payment_btn").append('<a href="{{url('transactions/create')}}" class="btn btn-sm btn-primary">Top up'+
                        '</a><button type="button" class="btn btn-xs btn-circle btn-stroke btn-warning">'+
                                    'OR</button><button type="button" data-target="#myModal" data-toggle="modal" class="btn btn-sm btn-success">'+
                                    'Pay with M-Pesa</button>');
                            showErrors('Failed: Insufficient account balance, pay with M-Pesa or Top Up Account');
                        }
                        else {
                            showErrors('Failed: The amount entered does not much our fare amount, try again');
                        }
                    }
                    else {
                        swal(
                                'Subscription successful',
                                'Your subcription reference number is ' + $("#t_ticket").html(),
                                'success'
                        ).then(function () {
                            var numberOfpassangers = $("#passangers").val();
                            if(numberOfpassangers >  1)
                            {
                                window.location='{{url('monthly-plan/passangers/')}}';
                            }
                            else{
                                window.location='{{url('/')}}';
                            }

                        });
                    }

                },
                error: function (response) {
                    if(response['responseJSON']['reference'])
                    {
                        showErrors('This subscription plan number, has been taken. Go to your My Subscription to confirm if you subscribed before subscribe again');
                    }
                    else if(response['responseJSON']['pickup'])
                    {
                        showErrors('Pick up station detail error, try again');
                    }
                    else if(response['responseJSON']['destination'])
                    {
                        showErrors('Destination station detail error, try again');
                    }
                    else if(response['responseJSON']['fare'])
                    {
                        showErrors('Fare detail error, try again');
                    }
                    else {
                        showErrors('Sorry for this, seems like something went wrong. Try again later');
                    }
                }

            })
        }
        function addRoute(route_id,routes) {
            if(route_id.value.length < 1)
            {
                $("#pickup").empty().append('<option value="" hidden>Select pickup</option>');
                $("#pickup").select2();
                $("#destination").empty();
                $("#destination").append('<option value="" hidden>Select destination</option>');
                $("#destination").select2();
            }
            else {
                $.each(routes, function (index, value) {
                    if (route_id.value == value['id']) {

                        $("#pickup").empty();

                        $("#destination").empty();

                        var data = '';
                        $.each(value['stages'], function (stage_index, stage_value) {
                            data = data + '<option value="' + stage_value['id'] + '">' +
                                    stage_value['name'] + '</option>';
                        })

                        $("#pickup").append('<option value="" hidden>Select pickup</option>'+ data);
                        $("#pickup").select2();
                        $("#destination").append('<option value="" hidden>Select destination</option>'+ data);
                        $('#destination').select2();
                    }
                });
            }

            if(route_id.value.length > 0)
            {
                $("#t_route").html($("#route option:selected").text());
            }
            else {
                $("#t_route").html('Select route');
            }

            $("#t_pickup").html('Select pick up station');
            $("#t_destination").html('Select destination station');
            $("#t_fare").text('0.0');
        }

        function stationUpdate(valueX) {
            if((valueX == 'pickup' || valueX == 'monthly_plan_type' || valueX == 'plan_type') && $("#destination").val().length > 0 )
            {
                addDestination();
            }
            if($('#'+valueX).val().length > 0)
            {
                $('#t_'+valueX).html($('#'+valueX+' option:selected').text());
            }
            else {
                $('#t_'+valueX).html('Select ' + valueX + ' station');
                $("#t_fare").text('0.0');
            }
        }

        function checkError() {
            if($("#route").val() == null)
            {
                showErrors('Select route');
            }
        }
    </script>
@endsection