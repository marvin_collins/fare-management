<!DOCTYPE html>
<html class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>KBS - online fare management</title>
    <link href="{{asset('css/app/sweetalert2.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/vendor/all.css')}}" rel="stylesheet">
    <link href="{{asset('css/app/app.css')}}" rel="stylesheet">
    <link href="{{asset('css/app/typed.css')}}" rel="stylesheet">
   <link href="{{asset('css/app/datepicker.min.css')}}" rel="stylesheet">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
</head>
<body>
@include('partials.topbarmenu')
@yield('userdetails')
@yield('content')
{{--@include('partials.footer')--}}
<script src="{{asset('js/app/sweetalert2.min.js')}}"></script>
{{--<script src="{{asset('js/app/datepicker.min.js')}}"></script>--}}
<script>
    var colors = {
        "danger-color": "#e74c3c",
        "success-color": "#81b53e",
        "warning-color": "#f0ad4e",
        "inverse-color": "#2c3e50",
        "info-color": "#2d7cb5",
        "default-color": "#6e7882",
        "default-light-color": "#cfd9db",
        "purple-color": "#9D8AC7",
        "mustard-color": "#d4d171",
        "lightred-color": "#e15258",
        "body-bg": "#f6f6f6"
    };
    var config = {
        theme: "html",
        skins: {
            "default": {
                "primary-color": "#42a5f5"
            }
        }
    };
</script>
<script src="{{asset('js/vendor/all.js')}}"></script>
<script src="{{asset('js/app/app.js')}}"></script>
<script src="{{asset('js/app/datepicker.min.js')}}"></script>
<script src="{{asset('js/app/typed.js')}}"></script>

@yield('scripts')
</body>

</html>