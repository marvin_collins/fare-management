@extends('layout')

@section('content')
    <div class="container">
        <div class="row" style="margin-top: 20px">
            @foreach($errors as $error)
                <p>{{$error}}</p>
            @endforeach
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><h3>Top Up Account</h3></div>
                    <div class="panel-body">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h4>Instructions</h4>
                                <ol>
                                    <li>Go to M-PESA Menu</li>
                                    <li>Select Pay Bill</li>
                                    <li>Enter 552552 as the Business Number</li>
                                    <li>Enter {{$wallet->account_number}} as the Account Number</li>
                                    <li>Enter the value amount to pay eg 3000 (NO COMMAS)</li>
                                    <li>Enter your M-PESA PIN</li>
                                    <li>Then send the request</li>
                                    <li>You will receive an SMS confirming the transaction</li>
                                    <li>Enter the M-PESA REFERENCE NUMBER below</li>
                                    <li>Click finish button below to finish</li>
                                </ol>
                            </div>
                        </div>

                        <form class="form-horizontal" role="form" method="POST" action="{{ route('transactions.store') }}">
                            {{ csrf_field() }}
                            <div class="col-md-10 col-md-offset-1">
                                <div class="form-group form-control-material static required {{ $errors->has('topedamount') ? ' has-error' : '' }}">
                                    <input id="topedamount" type="number" min="20" class="form-control" name="topedamount" value="{{ old('topedamount') }}" required autofocus>
                                    <label for="topedamount">Amount
                                    </label>
                                </div>
                                <div class="form-group form-control-material static required {{ $errors->has('reference') ? ' has-error' : '' }}">
                                    <input id="reference" type="text" class="form-control text-uppercase" name="reference" value="{{ old('reference') }}" required>
                                    <label for="reference">M-PESA REFERENCCE NUMBER
                                    </label>
                                </div>
                                <div class="form-group">
                                    {{--<div class="col-md-6 col-md-offset-3">--}}
                                    <button type="submit" class="btn pull-right btn-primary">
                                        Finish
                                    </button>
                                </div>
                                {{--</div>--}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
