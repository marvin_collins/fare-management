@extends('layout')
@include('partials.userdetailsbar')
@section('content')
    <div class="container">
    <div class="page-section">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="text-headline margin-none">My Account Statements</h4>
                        </div>
                        <div class="panel-body">
                        <table data-toggle="data-table" class="table table-responsive" cellspacing="0" >
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Reference No</th>
                                <th>Description</th>
                                <th>Type</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Date</th>
                                <th>Reference No</th>
                                <th>Description</th>
                                <th>Type</th>
                                <th class="text-right">Amount</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($transactions as $transaction)
                            <tr>
                                <td><button class="btn btn-sm primary">{{\Carbon\Carbon::parse($transaction->created_at)->format('d-M-Y H:m')}}</button></td>
                                <td class="text-uppercase">{{ucwords($transaction->reference)}}</td>
                                <td>{{$transaction->description}}</td>
                                <td>{{ucwords($transaction->type)}}</td>
                                <td class="text-right">Ksh : {{$transaction->amount}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- // Data table -->
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
        @include('partials.sidemenu')
    </div>
            </div>
        </div>
    </div>
    </div>
    @include('partials.footer')
@endsection