@extends('layout')
@section('content')
    <div class="parallax page-section bg-blue-300">
        <div class="container parallax-layer" data-opacity="true">
            <div class="media media-grid v-middle">
                <div class="media-left">
                    <span class="icon-block half bg-blue-500 text-white"><i class="fa fa-envelope">
                        </i></span>
                </div>
                <div class="media-body">
                    <h3 class="text-display-2 text-white margin-none">Contact us</h3>
                    <p class="text-white text-subhead">Feel free to visit or send us a message anytime.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="page-section parallax relative overflow-hidden">
        <img class="parallax-layer absolute top left" data-translate-when="inViewport" src="images/photodune-6745579-modern-creative-man-relaxing-on-workspace-m.jpg" alt="parallax image" />
        <div class="container" style="margin-top: 25px">
            <div class="panel margin-none panel-default paper-shadow max-width-800 h-center" data-z="0.5">
                <div class="panel-heading">
                    <h4 class="text-headline text-center">Hey! You have a word, contact us now.</h4>
                </div>
                <div class="panel-body">
                    <div class="col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4  style="color: white">You can email us directly.</h4>
                            </div>
                            <div class="panel-body">
                        <form>
                            <div class="form-group form-control-material">
                                <input class="form-control" type="text" id="fname" placeholder="First name" />
                                <label for="fname">First name:</label>
                            </div>
                            <div class="form-group form-control-material">
                                <input class="form-control" type="tel" id="lname" placeholder="Last name" />
                                <label for="lname">Last name:</label>
                            </div>
                            <div class="form-group form-control-material">
                                <input class="form-control" type="text" id="phone" placeholder="Phone" />
                                <label for="phone">Phone:</label>
                            </div>
                            <div class="form-group form-control-material">
                                <textarea class="form-control" id="message" placeholder="Your message"></textarea>
                                <label for="message">Your message:</label>
                            </div>
                            <div class="text-right">
                                <button class="btn btn-primary relative paper-shadow" data-z="0.5" data-hover-z="1" data-animated>Send message</button>
                            </div>
                        </form>
                            </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 style="color: white">Contact information</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <span class="fa-stack fa-lg fa-2x">
                                                <i class="fa fa-circle fa-stack-2x" ></i>
                                             <i class="fa fa-phone fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </div>
                                        <div class="col-sm-10">
                                             <h3 style="vertical-align: middle; float: none; display: inline-block">
                                                 Phone: 0704407117</h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <span class="fa-stack fa-lg fa-2x">
                                                <i class="fa fa-circle fa-stack-2x" ></i>
                                             <i class="fa fa-envelope fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </div>
                                        <div class="col-sm-10">
                                             <h3 style="vertical-align: middle; float: none; display: inline-block">
                                                 Email: info@travellers.co.ke</h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <span class="fa-stack fa-lg fa-2x">
                                                <i class="fa fa-circle fa-stack-2x" ></i>
                                             <i class="fa fa-briefcase fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </div>
                                        <div class="col-sm-10">
                                             <h3 style="vertical-align: middle; float: none; display: inline-block">
                                                 Career: career@travellers.co.ke
                                             </h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <span class="fa-stack fa-lg fa-2x">
                                                <i class="fa fa-circle fa-stack-2x" ></i>
                                             <i class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </div>
                                        <div class="col-sm-10">
                                             <h3 style="vertical-align: middle; float: none; display: inline-block">
                                                 Address: Eldoret, Kenya
                                             </h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <span class="fa-stack fa-lg fa-2x">
                                                <i class="fa fa-circle fa-stack-2x" ></i>
                                             <i class="fa fa-skype fa-stack-1x fa-inverse"></i>
                                            </span>
                                        </div>
                                        <div class="col-sm-10">
                                             <h3 style="vertical-align: middle; float: none; display: inline-block">
                                                 kenyatravellers
                                             </h3>
                                        </div>
                                    </div>
                                    <br>

                                </div>
                            </div>
                    </div>
                    <div class="col-md-12">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13419.559816608766!2d35.263069831399484!3d0.512027541625174!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb2d614bfd81e3a3d!2sMoi+Teaching+And+Referral+Hospital!5e0!3m2!1sen!2s!4v1485507314789" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>

                </div>
            </div>
            <br/>
        </div>
    </div>

    @include('partials.footer')
@endsection