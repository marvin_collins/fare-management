@extends('admin-layout')

@section('content')
    <div class="container-fluid">
        <div class="row" style="margin-top: 20px">


            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    @if($errors)
                        <div class="alert alert-danger" role="alert">
                            @foreach($errors->all() as $key => $error)
                                <p>{{$error}}</p>
                            @endforeach
                        </div>
                    @endif
                    <div class="panel-heading text-center"><h3>Add Bus Operator</h3></div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{route('bus-operators.store')}}">
                            {{ csrf_field() }}
                            <div class="col-md-12">
                                <div class="form-group form-control-material static required {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                    <label for="name">Operator Name</label>
                                </div>
                                <div class="form-group form-control-material static required {{ $errors->has('contact') ? ' has-error' : '' }}">
                                    <input id="contact" type="text" class="form-control" name="contact" value="{{ old('contact') }}" required >
                                    <label for="contact">Contact</label>
                                </div>
                                <div class="form-group form-control-material static required {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                    <label for="email">Email Address</label>
                                </div>
                                <div class="form-group form-control-material static required {{ $errors->has('number_of_buses') ? ' has-error' : '' }}">
                                    <input id="number_of_buses" min="1" type="number" class="form-control" name="number_of_buses" value="{{ old('number_of_buses') }}" required >
                                    <label for="number_of_buses">Number of Buses</label>
                                </div>
                                <div class="form-group static required">
                                    <label for="about">Abut Operator</label>
                                    <textarea name="about" id="about" cols="30" rows="5" class="summernote"></textarea>
                                </div>
                                <div class="form-group form-control-material static">
                                    <input id="images[]" type="file" multiple class="form-control" name="images" value="" >
                                    <label for="images">Images</label>
                                </div>
                                <br>

                                <div class="col-md-12">
                                    <button type="submit" class="btn btn_space pull-right btn-primary">
                                        Add
                                    </button>

                                        <a class="btn btn-danger" href="{{url()->previous()}}" >
                                            Cancel
                                        </a>
                                </div>
                            </div>
                            <div class="form-group">

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="/js/app.js"></script>
@endsection
