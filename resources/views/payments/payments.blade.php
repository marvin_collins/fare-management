@extends('layout')
@section('content')
    <div class="container">
        <div class="page-section">
            <div class="row">

                <div class="col-md-9">

                    <div class="panel panel-default paper-shadow" data-z="0.5">
                        <div class="panel-heading">
                            <div class="max-width-300 form-group daterangepicker-report">
                                <div class="form-control">
                                    <i class="fa fa-calendar fa-fw"></i>
                                    <span>{{\Carbon\Carbon::now()->format('d-M-Y')}}</span>
                                    <b class="caret"></b>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table text-subhead v-middle">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Reference</th>
                                    <th>Paid To Acc</th>
                                    <th>Description</th>
                                    <th>Type</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($payments as $payment)
                                    <tr>
                                        <td><button class="btn btn-sm primary">{{\Carbon\Carbon::parse($payment->created_at)->format('d-M-Y H:m')}}</button></td>
                                        <td class="text-uppercase">{{ucwords($payment->reference)}}</td>
                                        <td>{{ucwords($payment->account_number)}}</td>
                                        <td>{{$payment->description}}</td>
                                        <td>{{$payment->type}}</td>
                                        <td class="text-right"> Ksh : {{$payment->amount_paid}}</td>
                                        <td>{{ucwords($payment->status)}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="panel-footer">
                            Showing {{ $payments->firstItem() }} to {{ $payments->lastItem() }} of {{ $payments->total() }} entries
                            <div class="pull-right">
                                {!! $payments->links() !!}
                            </div>
                        </div>
                    </div>

                    <br/>
                    <br/>

                </div>
                <div class="col-md-3">

                    @include('partials.sidemenu')

                </div>

            </div>
        </div>
    </div>

@endsection