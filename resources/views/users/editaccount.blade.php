@extends('layout')
@section('content')
    <div class="container">
        <div class="page-section">
            <div class="row">
                <div class="col-md-9">

                    <!-- Tabbable Widget -->
                    <div class="tabbable paper-shadow relative" data-z="0.5">

                        <!-- Tabs -->
                        {{--<ul class="nav nav-tabs">--}}
                            {{--<li class="active"><a href="website-student-profile.html"><i class="fa fa-fw fa-lock"></i> <span class="hidden-sm hidden-xs">Manage Account</span></a></li>--}}
                            {{--<li><a href="website-student-billing.html"><i class="fa fa-fw fa-credit-card"></i> <span class="hidden-sm hidden-xs">Billing Details</span></a></li>--}}
                        {{--</ul>--}}
                        <!-- // END Tabs -->

                        <!-- Panes -->
                        <div class="tab-content">

                            <div id="account" class="tab-pane active">
                                <form method="POST" action="{{url('updateUserAccount')}}" class="form-horizontal">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Avatar</label>
                                        <div class="col-md-6">
                                            <div class="media v-middle">
                                                <div class="media-left">
                                                    <div class="icon-block width-100 bg-grey-100">
                                                        <i class="fa fa-photo text-light"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body">
                                                    <a href="#" class="btn btn-white btn-sm paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated> Add Image<i class="fa fa-upl"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="name" class="col-md-2 control-label">Full Name</label>
                                        <div class="col-md-9">
                                            <div class="form-control-material">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <input id="name" class="form-control" type="text" value="{{$user->name}}" name="name" required>
                                                    {{--<label for="national_id">National ID</label>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="username" class="col-md-2 control-label">Username</label>
                                        <div class="col-md-9">
                                            <div class="form-control-material">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    <input id="username" readonly class="form-control" type="text" value="{{$user->username}}" name="username" required>
                                                    {{--<label for="national_id">National ID</label>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="national_id" class="col-md-2 control-label">National ID</label>
                                        <div class="col-md-9">
                                            <div class="form-control-material">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-list-alt"></i></span>
                                                    {{--<input type="text" class="form-control" id="national_id" value="{{$user->national_id}}" placeholder="National ID">--}}
                                                    {{--<label for="national_id">National ID</label>--}}
                                                    <input id="national_id" class="form-control" type="text" value="{{$user->national_id}}" name="national_id" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone" class="col-md-2 control-label">Phone Number</label>
                                        <div class="col-md-9">
                                            <div class="form-control-material">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                    <input id="phone" class="form-control" type="text" value="{{$user->phone}}" name="phone" required>
                                                    {{--<label for="phone">Phone number</label>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-md-2 control-label">Email  Address</label>
                                        <div class="col-md-9">
                                            <div class="form-control-material">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                    <input id="email" class="form-control" type="text" value="{{$user->email}}" name="email" required>
                                                    {{--<label for="phone">Phone number</label>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--<div class="form-group">--}}
                                        {{--<div class="col-md-offset-2 col-md-6">--}}
                                            {{--<div class="checkbox checkbox-success">--}}
                                                {{--<input id="checkbox3" type="checkbox" checked="">--}}
                                                {{--<label for="checkbox3">Subscribe to our Newsletter</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="form-group margin-none">
                                        <div class="col-md-offset-2 col-md-10">
                                            <button type="submit" class="btn btn-primary paper-shadow relative" >Save Changes</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                        <!-- // END Panes -->

                    </div>
                    <!-- // END Tabbable Widget -->

                    <br/>
                    <br/>

                </div>
                <div class="col-md-3">
                    @include('partials.sidemenu')
                </div>

            </div>
        </div>
    </div>

@endsection