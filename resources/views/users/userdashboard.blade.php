@extends('layout')
@section('userdetails')
  @include('partials.userdetailsbar')
  @endsection
@section('content')
  <div class="container">

    <div class="page-section">
      <div class="row">

        <div class="col-md-9">

          <div class="row" data-toggle="isotope">

            <div class="item col-xs-12 col-lg-6">
              <div class="panel panel-default paper-shadow" data-z="0.5">
                <div class="panel-heading">
                  <h4 class="text-headline margin-none">E-Wallet</h4>
                  <p class="text-subhead text-light">Account number {{$wallet->account_number}}</p>
                </div>
                <ul class="list-group">
                  <li class="list-group-item media v-middle">
                    <div class="col-sm-8">
                      <a href="website-instructor-course-edit-course.html" class="text-subhead list-group-link">
                        Account Balance</a>
                    </div>
                    <div class="col-sm-4">
                      <div>
                        <h4 class="pull-right">Ksh : {{$wallet->balance}}</h4>
                      </div>
                    </div>
                  </li>
                  {{--<li class="list-group-item media v-middle">--}}
                    {{--<div class="media-body">--}}
                      {{--<a href="website-instructor-course-edit-course.html" class="text-subhead list-group-link">Angular in Steps</a>--}}
                    {{--</div>--}}
                    {{--<div class="media-right">--}}
                      {{--<div class="progress progress-mini width-100 margin-none">--}}
                        {{--<div class="progress-bar progress-bar-green-300" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">--}}
                        {{--</div>--}}
                      {{--</div>--}}
                    {{--</div>--}}
                  {{--</li>--}}
                </ul>
                <div class="panel-footer text-right">
                  <a href="{{url('statements')}}" class="btn btn-white paper-shadow relative" data-z="0" data-hover-z="1" data-animated>
                    View statements</a>
                  <a href="{{route('transactions.create')}}" class="btn btn-primary paper-shadow relative" data-z="0" data-hover-z="1" data-animated>
                    TOP UP ACCOUNT <i class="fa fa-plus"></i></a>
                </div>
              </div>
            </div>
            <div class="item col-xs-12 col-lg-6">
              <div class="panel panel-default paper-shadow" data-z="0.5">
              <div class="panel-heading">
                <div class="media v-middle">
                  <div class="media-body">
                      <h4 class="text-headline margin-none">News</h4>
                      <p class="text-subhead text-light">Latest news </p>
                  </div>
                  <div class="media-right">
                    <a href="website-instructor-courses.html" class="btn btn-warning paper-shadow relative" data-z="0" data-hover-z="1" data-animated>
                      View all news</a>
                  </div>
                </div>
              </div>
                <ul class="list-group">
                  <li class="list-group-item">
                    <div class="media v-middle margin-v-0-10">
                      <div class="media-body">
                        <p class="text-subhead">
                          <a href="#">
                            <img src="images/people/110/guy-2.jpg" alt="person" class="width-30 img-circle" />
                          </a> &nbsp;
                          <a href="#">mosaicpro</a>
                          <span class="text-caption text-light">38 min ago</span>
                        </p>
                      </div>
                      <div class="media-right">
                        <div class="width-50 text-right">
                          <a href="#" class="btn btn-white btn-xs"><i class="fa fa-reply"></i></a>
                        </div>
                      </div>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias, blanditiis eligendi expedita ipsam minus non numquam quidem reiciendis ut?</p>
                    <p class="text-light"><span class="caption"></span> <a href="app-student-course.html">Read now</a></p>
                  </li>
                  <li class="list-group-item">
                    <div class="media v-middle margin-v-0-10">
                      <div class="media-body">
                        <p class="text-subhead">
                          <a href="#">
                            <img src="images/people/110/guy-3.jpg" alt="person" class="width-30 img-circle" />
                          </a> &nbsp;
                          <a href="#">mosaicpro</a>
                          <span class="text-caption text-light">26 min ago</span>
                        </p>
                      </div>
                      <div class="media-right">
                        <div class="width-50 text-right">
                          <a href="#" class="btn btn-white btn-xs"><i class="fa fa-reply"></i></a>
                        </div>
                      </div>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias, blanditiis eligendi expedita ipsam minus non numquam quidem reiciendis ut?</p>
                    <p class="text-light"><span class="caption"></span> <a href="app-student-course.html">Read now</a></p>
                  </li>
                  <li class="list-group-item">
                    <div class="media v-middle margin-v-0-10">
                      <div class="media-body">
                        <p class="text-subhead">
                          <a href="#">
                            <img src="images/people/110/guy-2.jpg" alt="person" class="width-30 img-circle" />
                          </a> &nbsp;
                          <a href="#">mosaicpro</a>
                          <span class="text-caption text-light">52 min ago</span>
                        </p>
                      </div>
                      <div class="media-right">
                        <div class="width-50 text-right">
                          <a href="#" class="btn btn-white btn-xs"><i class="fa fa-reply"></i></a>
                        </div>
                      </div>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias, blanditiis eligendi expedita ipsam minus non numquam quidem reiciendis ut?</p>
                    <p class="text-light"><span class="caption"></span> <a href="app-student-course.html">Read now</a></p>
                  </li>
                </ul>
              </div>
            </div>
            <div class="item col-xs-12 col-lg-6">
              <div class="panel panel-default paper-shadow" data-z="0.5">
                <div class="panel-heading">
                  <div class="media v-middle">
                    <div class="media-body">
                      <h4 class="text-headline margin-none">Transactions</h4>
                      <p class="text-subhead text-light">Latest from statement</p>
                    </div>
                    <div class="media-right">
                      <a class="btn btn-success btn-flat" href="{{url('statements')}}">Statements</a>
                    </div>
                  </div>
                </div>
                <div class="table-responsive">
                  <table data-toggle="data-table" class="table" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                      <th>Date</th>
                      <th>Type</th>
                      <th>REF NO</th>
                      <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($transactions as $transaction)
                        <tr>
                          <td class="width-100 text-caption">
                            <div class="label label-grey-200 label-xs">{{\Carbon\Carbon::parse($transaction->created_at)
                            ->format('d-M-Y')}}</div>
                          </td>
                          <td>{{$transaction->type}}</td>
                          <td class="width-80 text-center text-uppercase"><a href="#">{{$transaction->reference}}</a></td>
                          <td class="width-50 text-center">{{$transaction->amount}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                {{--<div class="col-xs-12">--}}
                  {{--Showing {{ $transactions->firstItem() }} to {{ $transactions->lastItem() }} of {{ $transactions->total() }} entries--}}
                  {{--<div class="pull-right">--}}
                    {{--{!! $transactions->links() !!}--}}
                  {{--</div>--}}
                {{--</div>--}}
              </div>
            </div>
          </div>

          <br/>
          <br/>

        </div>
        <div class="col-md-3">

          @include('partials.sidemenu')
          <h4>Featured</h4>
          <div class="slick-basic slick-slider" data-items="1" data-items-lg="1" data-items-md="1" data-items-sm="1" data-items-xs="1">
            <div class="item">
              <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
                <div class="panel-body">
                  <div class="media media-clearfix-xs">
                    <div class="media-left">
                      <div class="cover width-90 width-100pc-xs overlay cover-image-full hover">
                        <span class="img icon-block s90 bg-default"></span>
                        <span class="overlay overlay-full padding-none icon-block s90 bg-default">
                        <span class="v-center">
                            <i class="fa fa-github"></i>
                        </span>
                        </span>
                        <a href="website-course.html" class="overlay overlay-full overlay-hover overlay-bg-white">
                          <span class="v-center">
                            <span class="btn btn-circle btn-white btn-lg"><i class="fa fa-graduation-cap"></i></span>
                          </span>
                        </a>
                      </div>
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading margin-v-5-3"><a href="website-course.html">Github Webhooks for Beginners</a></h4>
                      <p class="small margin-none">
                        <span class="fa fa-fw fa-star text-yellow-800"></span>
                        <span class="fa fa-fw fa-star text-yellow-800"></span>
                        <span class="fa fa-fw fa-star text-yellow-800"></span>
                        <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                        <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
                <div class="panel-body">
                  <div class="media media-clearfix-xs">
                    <div class="media-left">
                      <div class="cover width-90 width-100pc-xs overlay cover-image-full hover">
                        <span class="img icon-block s90 bg-primary"></span>
                        <span class="overlay overlay-full padding-none icon-block s90 bg-primary">
                        <span class="v-center">
                            <i class="fa fa-css3"></i>
                        </span>
                        </span>
                        <a href="website-course.html" class="overlay overlay-full overlay-hover overlay-bg-white">
                          <span class="v-center">
                            <span class="btn btn-circle btn-primary btn-lg"><i class="fa fa-graduation-cap"></i></span>
                          </span>
                        </a>
                      </div>
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading margin-v-5-3"><a href="website-course.html">Awesome CSS with LESS Processing</a></h4>
                      <p class="small margin-none">
                        <span class="fa fa-fw fa-star text-yellow-800"></span>
                        <span class="fa fa-fw fa-star text-yellow-800"></span>
                        <span class="fa fa-fw fa-star text-yellow-800"></span>
                        <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                        <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
                <div class="panel-body">
                  <div class="media media-clearfix-xs">
                    <div class="media-left">
                      <div class="cover width-90 width-100pc-xs overlay cover-image-full hover">
                        <span class="img icon-block s90 bg-lightred"></span>
                        <span class="overlay overlay-full padding-none icon-block s90 bg-lightred">
                        <span class="v-center">
                            <i class="fa fa-windows"></i>
                        </span>
                        </span>
                        <a href="website-course.html" class="overlay overlay-full overlay-hover overlay-bg-white">
                          <span class="v-center">
                            <span class="btn btn-circle btn-red-500 btn-lg"><i class="fa fa-graduation-cap"></i></span>
                          </span>
                        </a>
                      </div>
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading margin-v-5-3"><a href="website-course.html">Portable Environments with Vagrant</a></h4>
                      <p class="small margin-none">
                        <span class="fa fa-fw fa-star text-yellow-800"></span>
                        <span class="fa fa-fw fa-star text-yellow-800"></span>
                        <span class="fa fa-fw fa-star text-yellow-800"></span>
                        <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                        <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
                <div class="panel-body">
                  <div class="media media-clearfix-xs">
                    <div class="media-left">
                      <div class="cover width-90 width-100pc-xs overlay cover-image-full hover">
                        <span class="img icon-block s90 bg-brown"></span>
                        <span class="overlay overlay-full padding-none icon-block s90 bg-brown">
                        <span class="v-center">
                            <i class="fa fa-wordpress"></i>
                        </span>
                        </span>
                        <a href="website-course.html" class="overlay overlay-full overlay-hover overlay-bg-white">
                          <span class="v-center">
                            <span class="btn btn-circle btn-orange-500 btn-lg"><i class="fa fa-graduation-cap"></i></span>
                          </span>
                        </a>
                      </div>
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading margin-v-5-3"><a href="website-course.html">WordPress Theme Development</a></h4>
                      <p class="small margin-none">
                        <span class="fa fa-fw fa-star text-yellow-800"></span>
                        <span class="fa fa-fw fa-star text-yellow-800"></span>
                        <span class="fa fa-fw fa-star text-yellow-800"></span>
                        <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                        <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
                <div class="panel-body">
                  <div class="media media-clearfix-xs">
                    <div class="media-left">
                      <div class="cover width-90 width-100pc-xs overlay cover-image-full hover">
                        <span class="img icon-block s90 bg-purple"></span>
                        <span class="overlay overlay-full padding-none icon-block s90 bg-purple">
                        <span class="v-center">
                            <i class="fa fa-jsfiddle"></i>
                        </span>
                        </span>
                        <a href="website-course.html" class="overlay overlay-full overlay-hover overlay-bg-white">
                          <span class="v-center">
                            <span class="btn btn-circle btn-purple-500 btn-lg"><i class="fa fa-graduation-cap"></i></span>
                          </span>
                        </a>
                      </div>
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading margin-v-5-3"><a href="website-course.html">Modular JavaScript with Browserify</a></h4>
                      <p class="small margin-none">
                        <span class="fa fa-fw fa-star text-yellow-800"></span>
                        <span class="fa fa-fw fa-star text-yellow-800"></span>
                        <span class="fa fa-fw fa-star text-yellow-800"></span>
                        <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                        <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="panel panel-default paper-shadow" data-z="0.5" data-hover-z="1" data-animated>
                <div class="panel-body">
                  <div class="media media-clearfix-xs">
                    <div class="media-left">
                      <div class="cover width-90 width-100pc-xs overlay cover-image-full hover">
                        <span class="img icon-block s90 bg-default"></span>
                        <span class="overlay overlay-full padding-none icon-block s90 bg-default">
                        <span class="v-center">
                            <i class="fa fa-cc-visa"></i>
                        </span>
                        </span>
                        <a href="website-course.html" class="overlay overlay-full overlay-hover overlay-bg-white">
                          <span class="v-center">
                            <span class="btn btn-circle btn-white btn-lg"><i class="fa fa-graduation-cap"></i></span>
                          </span>
                        </a>
                      </div>
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading margin-v-5-3"><a href="website-course.html">Easy Online Payments with Stripe</a></h4>
                      <p class="small margin-none">
                        <span class="fa fa-fw fa-star text-yellow-800"></span>
                        <span class="fa fa-fw fa-star text-yellow-800"></span>
                        <span class="fa fa-fw fa-star text-yellow-800"></span>
                        <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                        <span class="fa fa-fw fa-star-o text-yellow-800"></span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
    @include('partials.footer')
  </div>
@endsection
