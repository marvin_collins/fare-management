<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $fillable = ['name','description','route'];

    public function stages()
    {
        return $this->hasMany(RouteStage::class,'route_id','id');
    }
}
