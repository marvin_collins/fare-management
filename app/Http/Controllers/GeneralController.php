<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Kbs\AThelper;
use Kbs\smsGateway;

class GeneralController extends Controller
{
    protected $wallet;
    protected $transaction;

    /**
     * GeneralController constructor.
     * @param $wallet
     * @param $transaction
     */
    public function __construct(Wallet $wallet, Transaction $transaction)
    {
        $this->wallet = $wallet;
        $this->transaction = $transaction;
    }

    public function index()
    {
//        $gatewaySms =  new AThelper();
//        $gatewaySms->getMessageDetails('+254728204682','Call text whatsapp ukiwa free, last text before nimalize my online credit');
        if(Auth::check())
        {
            return view('users.userdashboard')
                ->withWallet($this->wallet->where('user_id',Auth::user()->id)->first())
                ->withTransactions($this->transaction->where('user_id',Auth::user()->id)->get());
        }

//        alert()->message('testing','welcome');
        return view('mainpage');

    }
}
