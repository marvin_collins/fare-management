<?php

namespace App\Http\Controllers;

use App\Fare;
use App\Http\Requests\MonthlyPlanRequest;
use App\Http\Requests\PlanCostRequest;
use App\MonthlyPlan;
use App\MonthlyPlans;
use App\Payment;
use App\Plan;
use App\Transaction;
use App\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Kbs\TransactionRepo;

class PlanTransactionController extends Controller
{
    protected $fare;
    protected $wallet;
    protected $monthly_plan_type;
    protected $monthly_plan_subscription;
    protected $transaction;
    protected $payment;

    /**
     * PlanTransactionController constructor.
     * @param $fare
     * @param $wallet
     * @param $monthly_plan_type
     */
    public function __construct(Fare $fare, Wallet $wallet, Plan $monthly_plan_type, Transaction $transaction
    , Payment $payment, MonthlyPlan $monthlyPlan)
    {
        $this->fare = $fare;
        $this->wallet = $wallet;
        $this->monthly_plan_type = $monthly_plan_type;
        $this->transaction = $transaction;
        $this->monthly_plan_subscription = $monthlyPlan;
        $this->payment = $payment;
    }


    public function cost(PlanCostRequest $request)
    {
        $account_status = 'OK';
        $fare = $this->fare->where('from_id',$request->from_id)
            ->where('to_id',$request->to_id)->first()->fare;

        $total_cost = self::calculateAmount($request->monthly_plan_type_id, $request->plan_type, $fare, $request->passangers);

        if ($this->wallet->where('user_id',Auth::user()->id)->first()->balance < $total_cost)
        {
            $account_status = 'FAIL';
        }
        return ['fare'=>$total_cost,'status'=>$account_status];
    }

    public function monthlyPlanSubs(MonthlyPlanRequest $request)
    {
        $fare = $this->fare->where('from_id',$request->terminal_one)

            ->where('to_id',$request->terminal_two)->first()->fare;

        $account_details = $this->wallet->where('user_id',Auth::user()->id)
            ->first();

        $total_cost = self::calculateAmount($request->monthly_plan_type_id, $request->plan_type, $fare, $request->passangers);

        if($total_cost != ceil($request->amount))
        {
            return ['payment'=>'failed','error'=>'fare'];
        }

        if($account_details->balance < $total_cost)
        {
            return ['payment'=>'failed','error'=>'insufficient'];
        }

        $account_balance = $account_details->balance - $total_cost;

        $this->wallet->findorfail($account_details->id)->update(['balance' => $account_balance]);

        $user_id = Auth::user()->id;
        $reference = $request->reference;

        $transaction = new TransactionRepo($this->transaction,$this->payment);
        $transaction->createTransaction(
            [
                'user_id' => $user_id,
                'type' => Transaction::MONTHLY_PLAN_SUBS,
                'reference' => $reference,
                'service' => Transaction::MONTHLY_PLAN_SUBS,
                'description' => Transaction::MONTHLY_PLAN_SUBS,
                'from_acc' => $account_details->account_number,
                'to_acc' => env('MONTHLY_PLAN_SUBS'),
                'amount' => $total_cost,
                'status' => Transaction::COMPLETED
            ]);

        $transaction->createPayment([
            'amount_paid' =>$request->amount,
            'user_id' => $user_id,
            'type'=>Payment::SUBSCRIPTION,
            'status' => Payment::SUCCESS,
            'reference' => $reference,
            'account_number' => env('MONTHLY_PLAN_SUBS'),
            'description' => Payment::MONTHLY_PLAN_SUBSCRIPTION
        ]);

        $this->monthly_plan_subscription->create(
            [
                'user_id' => $user_id,
                'route_id' => $request->route_id,
                'plan_id' => $request->monthly_plan_type_id,
                'amount' => $total_cost,
                'reference' => $request->reference,
                'plan_type' => $request->plan_type,
                'terminal_one' => $request->terminal_one,
                'terminal_two' => $request->terminal_two,
                'status' => MonthlyPlan::PLAN_STATUS_ACTIVE,
                'plan_count' => count($this->monthly_plan_subscription->all())+1,
                'plan_end_date' => Carbon::parse($request->plan_end_date),
                'plan_start_date' => Carbon::parse($request->plan_start_date)
                //TODO:GET HOURS AND MINUTES FOR THE DATES
            ]
        );

        return ['payment'=>'success'];
    }

    private function calculateAmount($monthly_plan_type_id, $plan_type, $fare, $passangers)
    {
        $monthlt_plan = $this->monthly_plan_type->findorfail($monthly_plan_type_id);

        $total_days = (30 / 7) * $monthlt_plan->number_of_days;

        $total_cost = ceil($fare * $total_days * ($plan_type == MonthlyPlan::PLAN_TYPE_ONEWAY ? 1 : 2) * $passangers);

        return $total_cost;

    }
}
