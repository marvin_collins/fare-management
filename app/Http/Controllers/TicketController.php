<?php

namespace App\Http\Controllers;

use App\Fare;
use App\Http\Requests\BuyTickectRequest;
use App\Http\Requests\FareRequest;
use App\Payment;
use App\Route;
use App\Ticket;
use App\Transaction;
use App\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Kbs\TransactionRepo;

class TicketController extends Controller
{
    protected $route;
    protected $account;
    protected $fare;
    protected $ticket;
    protected $payment;
    protected $transaction;

    /**
     * TicketController constructor.
     * @param $route
     * @param $account
     * @param $fare
     * @param $ticket
     * @param $payment
     * @param $transaction
     */
    public function __construct(Route $route,Wallet $account,Fare $fare,Ticket $ticket,Payment $payment,Transaction $transaction)
    {
        $this->route = $route;
        $this->account = $account;
        $this->fare = $fare;
        $this->ticket = $ticket;
        $this->payment = $payment;
        $this->transaction = $transaction;
    }

    public function buyTicket()
    {
        return view('tickets.buy')
            ->withWallet($this->account->where('user_id',Auth::user()->id)->first())
            ->withRoutes($this->route->with('stages')->get())
            ->withTicket('KBS0020170'.(count($this->ticket->all()) + 1));
    }

    public function payMent(BuyTickectRequest $request)
    {
        $fare = $this->fare->where('from_id',$request->pickup)
            ->where('to_id',$request->destination)->first()->fare;
        $account_details = $this->account->where('user_id',Auth::user()->id)
            ->first();

        if($fare != $request->fare)
        {
            return ['payment'=>'failed','error'=>'fare'];
        }

        if($account_details->balance < $fare)
        {
            return ['payment'=>'failed','error'=>'insufficient'];
        }

        $account_balance = $account_details->balance - $fare;
        $this->account->findorfail($account_details->id)->update(['balance' => $account_balance]);

        $user_id = Auth::user()->id;
        $reference = $request->reference;

        $transaction = new TransactionRepo($this->transaction,$this->payment);
           $transaction->createTransaction(
            [
                'user_id' => $user_id,
                'type' => Transaction::TICKET,
                'reference' => $reference,
                'service' => Transaction::TICKET,
                'description' => Transaction::TICKET,
                'from_acc' => $account_details->account_number,
                'to_acc' => env('TICKET_ACCOUNT'),
                'amount' => $fare,
                'status' => Transaction::COMPLETED
            ]);

        $transaction->createPayment([
            'amount_paid' =>$request->fare,
            'user_id' => $user_id,
            'type'=>Payment::TICKET,
            'status' => Payment::SUCCESS,
            'reference' => $reference,
            'account_number' => env('TICKET_ACCOUNT'),
            'description' => Payment::TICKET_PAYMENT
        ]);

        $this->ticket->create([
                'user_id' =>$user_id,
                'route_id' => $request->route,
                'from_id' => $request->pickup,
                'to_id' => $request->destination,
                'ticket_count' => (count($this->ticket->all()) + 1),
                'reference' => $reference,
                'status' => Ticket::ACTIVE,
                'fare' => $fare
            ]);

        return ['payment'=>'success'];

    }

    public function getFare(FareRequest $request)
    {
        $account_status = 'OK';
        $fare = $this->fare->where('from_id',$request->from_id)
            ->where('to_id',$request->to_id)->first()->fare;
        if ($this->account->where('user_id',Auth::user()->id)->first()->balance < $fare)
        {
            $account_status = 'FAIL';
        }
        return ['fare'=>$fare,'status'=>$account_status];
    }

    public function myTickets()
    {
        return view('tickets.mytickets')
        ->withTickets($this->ticket->with(['fromStation','toStation','userRoute'])
        ->where('user_id',Auth::user()->id)->paginate(10));
    }
}
