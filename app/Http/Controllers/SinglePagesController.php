<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SinglePagesController extends Controller
{
    protected $transactions;

    /**
     * SinglePagesController constructor.
     * @param $transactions
     */
    public function __construct(Transaction $transactions)
    {
        $this->transactions = $transactions;
    }

    public function statements()
    {
        return view('transactions.statements')
            ->withTransactions($this->transactions->where('user_id',Auth::user()->id)->get());
    }
}
