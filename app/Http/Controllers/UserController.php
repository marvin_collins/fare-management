<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $user;

    /**
     * UserController constructor.
     * @param $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function editUserAccount()
    {
        return view('users.editaccount')
            ->withUser(Auth::user());
    }

    public function updateUserAccount(Request $request)
    {
        $this->user->findorfail(Auth::user()->id)->update($request->all());
        return redirect('/')->withSuccess('Profile updated successfully');
    }
}
