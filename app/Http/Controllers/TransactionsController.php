<?php

namespace App\Http\Controllers;

use App\Http\Requests\WalletRequest;
use App\Payment;
use App\Transaction;
use App\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Kbs\TransactionRepo;

class TransactionsController extends Controller
{
    protected $wallet;
    protected $transaction;
    protected $payment;

    /**
     * TransactionsController constructor.
     * @param $wallet
     * @param $transaction
     * @param $payment
     */
    public function __construct(Wallet $wallet,Transaction $transaction,Payment $payment)
    {
        $this->wallet = $wallet;
        $this->transaction = $transaction;
        $this->payment = $payment;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transactions.create')
            ->withWallet($this->wallet->where('user_id',Auth::user()->id)->first());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WalletRequest $request)
    {
        $wallet_data = $this->wallet->where('user_id',Auth::user()->id)->first();
        $this->wallet->findorfail($wallet_data->id)->update(
            [
                'balance' => $request->topedamount + $wallet_data->balance
            ]
        );

        $this->transaction->create(
            [
                'user_id' => Auth::user()->id,
                'type' => Transaction::ACCOUNT_TOP_UP,
                'reference' => $request->reference,
                'service' => Transaction::ACCOUNT_TOP_UP,
                'description' => Transaction::ACCOUNT_TOP_UP,
                'from_acc' => 'M-Pesa',
                'to_acc' => $wallet_data->account_number,
                'status' => Transaction::COMPLETED,
                'amount' => env('MPESA_AMOUNT')
            ]);

        if ($request->has('ajax'))
        {
            if ($request->amount > Wallet::where('user_id',Auth::user()->id)->first()->balance)
            {
                return ['message' => 'FAIL'];
            }

            return ['message'=>'success',
                'amount'=>Wallet::where('user_id',Auth::user()->id)->first()->balance];
        }
        return Redirect::to('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
