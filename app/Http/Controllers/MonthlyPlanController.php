<?php

namespace App\Http\Controllers;

use App\Fare;
use App\MonthlyPlan;
use App\Payment;
use App\Route;
use App\Ticket;
use App\Transaction;
use App\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MonthlyPlanController extends Controller
{
    protected $route;
    protected $account;
    protected $fare;
    protected $ticket;
    protected $payment;
    protected $transaction;
    protected $monthly_plan;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Route $route,Wallet $account,Fare $fare,Ticket $ticket,Payment $payment,
                                Transaction $transaction,MonthlyPlan $monthly_plan)
    {
        $this->route = $route;
        $this->account = $account;
        $this->fare = $fare;
        $this->ticket = $ticket;
        $this->payment = $payment;
        $this->transaction = $transaction;
        $this->monthly_plan = $monthly_plan;
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('plans.monthly-plan.create')
            ->withUser(Auth::user())
            ->withWallet($this->account->where('user_id',Auth::user()->id)->first())
            ->withRoutes($this->route->with('stages')->get())
            ->withTicket('KBS0020170-S'.(count($this->monthly_plan->all()) + 1));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
