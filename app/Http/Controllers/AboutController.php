<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function whoWeAre()
    {
        return view('about-us.about');
    }

    public function ourTeam()
    {
        return view('about-us.team');
    }
}
