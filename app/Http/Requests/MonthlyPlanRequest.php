<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MonthlyPlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'route_id' => 'required',
            'monthly_plan_type_id'=> 'required',
            'amount'=> 'required',
            'reference'=> 'required|unique:monthly_plans',
            'plan_type'=> 'required',
            'terminal_one'=> 'required',
            'terminal_two'=> 'required',
            'plan_end_date'=> 'required',
            'plan_start_date'=> 'required'
        ];
    }
}
