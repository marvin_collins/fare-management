<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    const ACTIVE = 'active';
    const INACTIVE = 'inactive';
    const INVALID = 'invalid';

    protected $fillable = ['user_id','route_id','from_id','to_id','ticket_count',
    'reference','status','fare'];

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function userRoute()
    {
        return $this->hasOne(Route::class,'id','route_id');
    }

    public function fromStation()
    {
        return $this->hasOne(RouteStage::class,'id','from_id');
    }

    public function toStation()
    {
        return $this->hasOne(RouteStage::class,'id','to_id');
    }
}
