<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    const TICKET = 'Ticketing';
    const SUBSCRIPTION = 'Subscription';
    const PLAN = 'Plan';
    const RECEIVED = 'Received';
    const SUCCESS ='Successful';
    const INVALID = 'Invalid';
    const REVERSED = 'Reversed';
    const TICKET_PAYMENT = 'Ticket payment';
    const MONTHLY_PLAN_SUBSCRIPTION = 'Ticket payment';
    const PLAN_PAYMENT = 'Plan payment';

    protected $fillable = ['amount_paid','user_id','type','status','reference','account_number','description'];

    public function user()
    {
        $this->belongsTo('App/User');
    }

}
