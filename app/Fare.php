<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fare extends Model
{
    protected $fillable =  ['from_id','to_id','fare'];
}
