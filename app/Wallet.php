<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $fillable = ['user_id','account_number','balance'];

    public function users()
    {
        return $this->hasOne(User::class);
    }
}
