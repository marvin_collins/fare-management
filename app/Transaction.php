<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    const ACCOUNT_TOP_UP = 'Account top up';
    const TICKET = 'Ticket purchase';
    const MONTHLY_PLAN_SUBS = 'Monthly plan subscription';
    const COMPLETED = 'successful';
    const PROCESSING = 'processing';
    const REVERSAL ='reversed';
    protected $fillable = ['user_id','type','reference','service','description'
    ,'from_acc','to_acc','amount','status'];

    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
