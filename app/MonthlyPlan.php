<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthlyPlan extends Model
{
    const PLAN_STATUS_ACTIVE = 'Active';
    const PLAN_STATUS_CANCELLED = 'Cancelled';
    const PLAN_STATUS_INACTIVE = 'Inactive';
    const PLAN_TYPE_ONEWAY = 'oneway';
    const PLAN_TYPE_RETURN = 'return';

    protected $fillable = ['user_id','route_id','plan_id','amount','reference','plan_type',
        'terminal_one','terminal_two','status','plan_count','plan_end_date'
        ,'plan_start_date'];

    public function user()
    {
        $this->belongsTo(User::class,'user_id','id');
    }

    public function route()
    {
        $this->belongsTo(Route::class,'route_id','id');
    }

    public function plan()
    {
        //TODO:add on admin
    }

    public function routeStage()
    {
        //TODO: add
    }
}
