<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RouteStage extends Model
{
    protected $fillable = ['route_id','name','location'];

    public function route()
    {
        return $this->belongsTo('App/Route');
    }
}
