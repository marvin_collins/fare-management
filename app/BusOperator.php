<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusOperator extends Model
{
    protected $fillable = ['name','user_id','contact','email','about','number_of_buses','images'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
