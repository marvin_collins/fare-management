<?php
/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 2/12/17
 * Time: 11:25 PM
 */

namespace Kbs;


use App\Payment;
use App\Transaction;

class TransactionRepo
{
    protected $transaction;
    protected $payment;

    /**
     * TransactionRepo constructor.
     * @param $transaction
     */
    public function __construct(Transaction $transaction,Payment $payment)
    {
        $this->transaction = $transaction;
        $this->payment = $payment;
    }

    public function createTransaction($data)
    {
        $this->transaction->create($data);
    }

    public function createPayment($data)
    {
        $this->payment->create($data);
    }
}