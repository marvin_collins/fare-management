<?php
/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 2/11/17
 * Time: 10:23 AM
 */

namespace Kbs;
use AfricasTalkingGateway;

require_once('AfricasTalkingGateway.php');


class AThelper
{
    private $gateway;

    /**
     * ATdetails constructor.
     */
    public function __construct()
    {
        $this->gateway = new AfricasTalkingGateway(env('AFRICAISTALKING_USERNAME'),env('AFRICAISTALKING_API_KEY'));
    }

    public function getMessageDetails($recipient,$message)
    {
        $this->sendMessage(['phone_number'=>$recipient,'message'=>$message]);
    }

    private function sendMessage($data)
    {
        try{
            $result = $this->gateway->sendMessage($data['phone_number'],$data['message']);
            dd($result);
        }

        catch (\AfricasTalkingGatewayException $exception)
        {
            dd($exception->getMessage());
        }
    }


}